export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      title: true,
      name: 'Manage Accreditation Tool',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      },
      auth: {
        roles: ['SuperAdmin']
      },
    },
    {
      name: 'Preset Areas',
      url: '/preset-areas',
      icon: 'fa fa-chevron-circle-right',
      auth: {
        roles: ['SuperAdmin']
      },
      children: [
        {
          name: 'Areas',
          url: '/preset-areas/list',
          auth: {
            roles: ['SuperAdmin']
          }

        },
        {
          name: 'Create Area',
          url: '/preset-areas/create',
          auth: {
            roles: ['SuperAdmin']
          }
        }
      ]
    },
    {
      name: 'Departments',
      url: '/departments/list',
      icon: 'fa fa-sitemap',
      auth: {
        roles: ['SuperAdmin']
      }
    },
    {
      title: true,
      name: 'Manage Users',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      },
      auth: {
        roles: ['SuperAdmin']
      },
    },
     {
      name: 'Users',
      url: '/users',
      icon: 'fa fa-users',
      auth: {
        roles: ['SuperAdmin']
      }
    },
    // {
    //   name: 'Roles',
    //   url: '/roles',
    //   icon: 'fa fa-sitemap',
    //   auth: {
    //     roles: ['SuperAdmin']
    //   }
    // },
    // {
    //   title: true,
    //   name: 'UI elements',
    //   class: '',
    //   wrapper: {
    //     element: '',
    //     attributes: {}
    //   }
    // },
    // {
    //   name: 'Components',
    //   url: '/components',
    //   icon: 'icon-puzzle',
    //   children: [
    //     {
    //       name: 'Buttons',
    //       url: '/components/buttons',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Social Buttons',
    //       url: '/components/social-buttons',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Cards',
    //       url: '/components/cards',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Forms',
    //       url: '/components/forms',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Modals',
    //       url: '/components/modals',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Switches',
    //       url: '/components/switches',
    //       icon: 'icon-puzzle'
    //     },
    //     {
    //       name: 'Tables',
    //       url: '/components/tables',
    //       icon: 'icon-puzzle'
    //     }
    //   ]
    // },
    // {
    //   name: 'Icons',
    //   url: '/icons',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Font Awesome',
    //       url: '/icons/font-awesome',
    //       icon: 'icon-star',
    //       badge: {
    //         variant: 'secondary',
    //         text: '4.7'
    //       }
    //     },
    //     {
    //       name: 'Simple Line Icons',
    //       url: '/icons/simple-line-icons',
    //       icon: 'icon-star'
    //     }
    //   ]
    // },
    // {
    //   name: 'Widgets',
    //   url: '/widgets',
    //   icon: 'icon-calculator',
    //   badge: {
    //     variant: 'primary',
    //     text: 'NEW'
    //   }
    // },
    // {
    //   name: 'Charts',
    //   url: '/charts',
    //   icon: 'icon-pie-chart'
    // },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Extras'
    // },
    // {
    //   name: 'Pages',
    //   url: '/pages',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/pages/login',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Register',
    //       url: '/pages/register',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 404',
    //       url: '/pages/404',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 500',
    //       url: '/pages/500',
    //       icon: 'icon-star'
    //     }
    //   ]
    // }
  ]
}
