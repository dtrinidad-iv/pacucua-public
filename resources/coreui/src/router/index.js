import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios';
import VueAxios from 'vue-axios';

// Containers
import Full from '../containers/Full'

// Views
import Dashboard from '../views/Dashboard'
import Charts from '../views/Charts'
import Widgets from '../views/Widgets'


// Views - Components
import Buttons from '../views/components/Buttons'
import SocialButtons from '../views/components/SocialButtons'
import Cards from '../views/components/Cards'
import Forms from '../views/components/Forms'
import Modals from '../views/components/Modals'
import Switches from '../views/components/Switches'
import Tables from '../views/components/Tables'


// Views - Icons
import FontAwesome from '../views/icons/FontAwesome'
import SimpleLineIcons from '../views/icons/SimpleLineIcons'

// Views - Pages
import Page403 from '../views/pages/Page403'
import Page404 from '../views/pages/Page404'
import Page500 from '../views/pages/Page500'
import Login from '../views/pages/Login'
import Register from '../views/pages/Register'


// Admin Entities
import Categories from '../views/categories/Category'


// Areas
import AreaLists from '../views/areas/AreaLists'
import CreateArea from '../views/areas/CreateArea'
import AreaPreview from '../views/areas/AreaPreview'
import Area from '../views/areas/area'

//Departments
import DepartmentLists from '../views/departments/DepartmentLists'

//Programs
import ProgramLists from '../views/programs/ProgramLists'
import AdminProgramLists from '../views/admins/ProgramList'

//Preset Areas
import PresetAreaList from '../views/preset-areas/PresetAreaList'
import PresetArea from '../views/preset-areas/PresetArea'
import CreatePresetArea from '../views/preset-areas/CreatePresetArea'
import AddPresetArea from '../views/preset-areas/AddPresetArea'
import PresetAreaPreview from '../views/preset-areas/PresetAreaPreview'

//Admin

import AdminAccreditationSummary from '../views/admins/AccreditationSummary'
import AdminAccreditationArea from '../views/admins/Area'
import AdminLanding from '../views/admins/Landing'

// Accreditor
import AccreditationSummary from '../views/accreditors/AccreditationSummary'
import AccreditationArea from '../views/accreditors/Area'
import AccreditationLanding from '../views/accreditors/Landing'

// Evaluator
import EvaluationDone from '../views/evaluators/EvaluationDone'
import AreaEvaluation from '../views/evaluators/Evaluation'
import EvaluationScale from '../views/evaluators/EvaluationScale'

// User Management
import Users from '../views/users/Users'
import Roles from '../views/roles/Roles'

Vue.use(Router)
Vue.use(VueAxios, axios)
axios.defaults.baseURL = '/api'



export default new Router({
  mode: 'history', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/login',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta: {
           auth: true
          }
        },
        {
          path: 'evaluation/departments/:department_id/programs/:program_id',
          name: 'Evaluation',
          redirect: 'evaluation/:program_id/about',
          component: {
            render (c) { return c('router-view') }
          },
          meta: {
           auth: {
              roles: ['Accreditor','Admin']
            }
          },
          children:
            [
              
              {

                path: 'accreditation-summary/view',
                name: 'Accreditation Summary',
                component: AccreditationSummary,
                meta: {
                 auth: {
                    roles: ['Accreditor','Admin']
                  }
                }
              },
              {

                path: 'area/:area_id',
                name: 'Accreditation Area',
                component: AccreditationArea,
                meta: {
                 auth: {
                    roles: ['Accreditor','Admin']
                  }
                }
              },
              {
                path: 'areas/:area_id/evaluate',
                name: 'Area Evaluation',
                component: AreaEvaluation,
                meta: {
                  auth: {
                    roles: ['Evaluator']
                  }
                }
              },
              {
                path: 'areas/:area_id/evaluation-scale',
                name: 'Evaluation Scale',
                component: EvaluationScale,
                meta: {
                  auth: {
                    roles: ['Evaluator']
                  }
                }
              },
              {
                path: 'areas/:area_id/evaluation-done',
                name: 'Evaluation Done',
                component: EvaluationDone,
                meta: {
                  auth: {
                    roles: ['Evaluator']
                  }
                }
              },
            ]
        },
        {
          path: 'users',
          name: 'Users',
          component: Users,
          meta: {
            auth: {
              roles: ['SuperAdmin','Admin']
            }
          }
        },
        // {
        //   path: 'roles',
        //   name: 'Roles',
        //   component: Roles,
        //   meta: {
        //     auth: {
        //       roles: ['SuperAdmin']
        //     }
        //   }
        // },
        {
          path: 'preset-areas',
          name: 'Preset Areas',
          redirect: '/preset-areas/list',
          component: {
            render (c) { return c('router-view') }
          },
          meta: {
            auth: {
              roles: ['SuperAdmin']
            }
          },
          children:
            [
            {
              path: 'list',
              name: 'Preset Area List',
              component: PresetAreaList,
              meta: {
                auth: {
                  roles: ['SuperAdmin']
                }
              }
            },
            {
              path: 'create',
              name: 'Create Preset Area',
              component: CreatePresetArea,
              meta: {
                auth: {
                  roles: ['SuperAdmin']
                }
              }
            },
            {
              path: ':area_id',
              name: 'Preset Area',
              component: PresetArea,
              meta: {
                auth: {
                  roles: ['SuperAdmin']
                }
              }
            },
            {
              path: ':area_id/preview',
              name: 'Preset Area Preview',
              component: PresetAreaPreview,
              meta: {
                auth: {
                  roles: ['SuperAdmin','Admin']
                }
              }
            },
          ]
        },

        {
          path: 'departments',
          name: 'Departments',
          redirect: '/departments/list',
          component: {
            render (c) { return c('router-view') }
          },
          meta: {
            auth: {
                    roles: ['SuperAdmin']
                  }
          },
          children:
            [
              {
                path: 'list',
                name: 'Department Lists',
                component: DepartmentLists,
                props: true,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                }
              },
              {
                path: ':department_id/programs',
                name: 'Programs',
                component: ProgramLists,
                props: true,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                },
              },
              {
                path: ':department_id/programs/:program_id/areas',
                name: 'Area Lists',
                component: AreaLists,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                }
              },
               {
                path: ':department_id/programs/:program_id/create-areas',
                name: 'Create Area',
                component: CreateArea,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                }
              },
               {
                path: ':department_id/programs/:program_id/add-preset-areas',
                name: 'Add Preset Area',
                component: AddPresetArea,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                }
              },
             
              {
                path: ':department_id/programs/:program_id/areas/:area_id',
                name: 'Area',
                component: Area,
                meta: {
                  auth: {
                    roles: ['SuperAdmin']
                  }
                }
              },
              {
                path: ':department_id/programs/:program_id/areas/:area_id/preview',
                name: 'Area Preview',
                component: AreaPreview,
                meta: {
                  auth: {
                    roles: ['SuperAdmin','Admin']
                  }
                }
              },
            ]
        },
        {
          path: 'departments/:department_id/programs/view',
          name: 'Program List - Admin',
          component: AdminProgramLists,
          props: true,
          meta: {
            auth: {
              roles: ['Admin']
            }
          },
          
        },
        {
          path: ':program_id/accreditation-areas',
          name: 'Accreditation Areas - Admin',
          component: AdminLanding,
          props: true,
          meta: {
            auth: {
              roles: ['Admin']
            }
          }
        },
        {
          path: 'accreditation-areas/:area_id',
          name: 'Area - Admin',
          component: AdminAccreditationArea,
          props: true,
          meta: {
            auth: {
              roles: ['Admin']
            }
          }
        },
         {
          path: 'programs/:program_id/accreditation-summary',
          name: 'Accreditation Summary - Admin',
          component: AdminAccreditationSummary,
          props: true,
          meta: {
            auth: {
              roles: ['Admin']
            }
          }
        },
        {
          path: 'categories',
          name: 'Categories',
          component: Categories
        },
        {
          path: 'charts',
          name: 'Charts',
          component: Charts
        },
        {
          path: 'widgets',
          name: 'Widgets',
          component: Widgets
        },
        {
          path: 'components',
          redirect: '/components/buttons',
          name: 'Components',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'buttons',
              name: 'Buttons',
              component: Buttons
            },
            {
              path: 'social-buttons',
              name: 'Social Buttons',
              component: SocialButtons
            },
            {
              path: 'cards',
              name: 'Cards',
              component: Cards
            },
            {
              path: 'forms',
              name: 'Forms',
              component: Forms
            },
            {
              path: 'modals',
              name: 'Modals',
              component: Modals
            },
            {
              path: 'switches',
              name: 'Switches',
              component: Switches
            },
            {
              path: 'tables',
              name: 'Tables',
              component: Tables
            }
          ]
        },
        {
          path: 'icons',
          redirect: '/icons/font-awesome',
          name: 'Icons',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'font-awesome',
              name: 'Font Awesome',
              component: FontAwesome
            },
            {
              path: 'simple-line-icons',
              name: 'Simple Line Icons',
              component: SimpleLineIcons
            }
          ]
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Signin',
          component: Login
        },
        {
          path: 'register',
          name: 'Signup',
          component: Register
        }
      ]
    },
   {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      auth: false
    }
   },
   {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      auth: false
    }
  },
  {
    path: '/404',
    name: '404',
    component: Page404,
  },
  {
    path: '/403',
    name: '403',
    component: Page403,
  },
  {
    path: '*',
    redirect: '/404',
  },
  ]
})
