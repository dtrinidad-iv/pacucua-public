export default {
  items: [
    {
      name: 'Programs',
      url: '/programs',
      icon: 'fa fa-circle',
      auth: {
        roles: ['Admin']
      }
    },
    {
      name: 'User',
      url: '/users',
      icon: 'fa fa-circle',
      auth: {
        roles: ['Admin']
      }
    }
   
  ]
}
