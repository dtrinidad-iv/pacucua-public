<?php

namespace App\Http\Controllers\Admin;

use App\Models\Traits\Authorizable;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\Gym;
class GymController extends Controller
{

  use Authorizable;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $r = $request;

      if (isset($r["sort"])){
         $sort = explode("|",$r["sort"]);
       }

       if (isset($r["filter"])) {
         $gyms = Gym::where('name', 'like', '%' . $r["filter"] . '%')->orWhere('email', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
       }else if(!isset($r["sort"])){
            $gyms = Gym::all();
            return response()->json(compact('gyms'));
       }else{
         $gyms = Gym::orderBy( $sort[0] ,$sort[1])->paginate(5);
       }

       return $gyms;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
     $this->validate($request, [
        'name' => 'required',
        'email' => 'nullable|email',
        'owners.*.name' => 'required',
        'owners.*.name' => 'required',
        'owners.*.email' => 'required|email|unique:owners',
        'owners.*.contact' => 'required',
        'owners.*.password' => 'required',
      ],
      ['name.required' => 'The name field is required.',
       'owners.*.name.required' => 'The owners name field is required.',
       'owners.*.email.required' => 'The owners email field is required.',
       'owners.*.email.email' => 'The owners email  is invalid.',
       'owners.*.contact.required' => 'The owners contact field is required.',
       'owners.*.password.required' => 'The owners password field is required.'
      ]
     );

     \DB::transaction(function() use ($request)
     {

       $input = $request;
       $gym_id =  \DB::table('gyms')->insertGetId([
              "name" => $input['name'],
              "code" => sha1(uniqid()),
              "description" => $input['description'],
              "email" => $input['email'],
              "contact" => $input['contact'],
              "address" => $input['address'],
              "created_at" => date("Y-m-d H:i:s")
            ]);

       foreach ($input['owners'] as $owner) {


          $user = User::create([
            "name" => $owner['name'],
            "gym_id" => $gym_id,
            "email" => $owner['email'],
            "password" => bcrypt($owner['password'])
          ]);

          $roles = Role::where('name', '=' ,'Owner')->firstOrFail();

          // check for current role changes
          if( ! $user->hasAllRoles( $roles ) ) {
              // reset all direct permissions for user
              $user->permissions()->sync([]);
          } else {
              // handle permissions
              $user->syncPermissions($permissions);
          }

          $user->syncRoles($roles);

          \DB::table('owners')->insert([
                 "gym_id" => $gym_id,
                 "user_id" => $user['id'],
                 "name" => $owner['name'],
                 "email" => $owner['email'],
                 "contact" => $owner['contact'],
               ]);

       }

     });

     return response('success', 200)
                       ->header('Content-Type', 'application/json');


  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }


}
