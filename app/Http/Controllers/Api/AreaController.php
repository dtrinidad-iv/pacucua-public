<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Areas\Repositories\AreaRepository;
use App\Models\Areas\Repositories\AreaRepositoryInterface;
use App\Models\Areas\Requests\CreateAreaRequest;
use App\Models\Areas\Requests\UpdateAreaRequest;
use Illuminate\Http\Request;
use App\Models\Areas\Area;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AreaController extends Controller
{

    // use Authorizable;

    /**
     * @var AreaRepositoryInterface
     */
    private $AreaRepo;

    /**
     * AreaController constructor.
     *
     * @param AreaRepositoryInterface $areaRepository
     */
    public function __construct(AreaRepositoryInterface $AreaRepo)
    {
        $this->AreaRepo = $AreaRepo;
    }


    public function index(Request $request)
    {
      $areas = $this->AreaRepo->getAllAreas($request);

      return response()->json($areas);
    }

    /**
     * @param CreateAreaRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAreaRequest $request)
    {


         $AreaRepo = new AreaRepository(new Area);
         $area = $AreaRepo->createArea($request->all());

         return response()->json([
             'success' => 'area_created',
             'message' => 'Area Successfully Created',
             'data' => $area
         ],201);


    }

    public function show($id)
    {

      $AreaRepo = new AreaRepository(new Area);
      $area = $AreaRepo->findAreaById($id);
      return response()->json($area);

    }

    public function update(UpdateAreaRequest $request, $id)
    {   
          $area = $this->AreaRepo->findAreaById($id);

          $update = new AreaRepository($area);
          $data = $update->updateArea($request->all());

           return response()->json([
               'success' => 'area_updated',
               'message' => 'Area Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $area = $this->AreaRepo->findAreaById($id);

        $delete = new AreaRepository($area);
        $data = $delete->deleteArea();

        return response()->json([
            'success' => 'area_deleted',
            'message' => 'Category Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function getAreasByProgram($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $area = $AreaRepo->getAreasByProgram($id);

        return response()->json($area);
    }

    public function deleteAreaDivision($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteAreaDivision($id);

        return response()->json([
            'success' => 'area_division_deleted',
            'message' => 'Area Division Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deleteDivisionSubset($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteDivisionSubset($id);

         return response()->json([
            'success' => 'area_deleted',
            'message' => 'Area Division Subset Successfully Deleted',
             'data' => $data
        ],200);
    }


    public function getAreaDivision($id)
    {
      $AreaRepo = new AreaRepository(new Area);
      $areaDivision = $AreaRepo->getAreaDivision($id);

      return response()->json($areaDivision);
    }

    public function addPresetArea(Request $request)
    {
         $AreaRepo = new AreaRepository(new Area);
         $area = $AreaRepo->addPresetArea($request);

         return response()->json([
             'success' => 'area_created',
             'message' => 'Area Successfully Added',
             'data' => $area
         ],201);

    }

    public function deleteAreaDivisionTable($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteAreaDivisionTable($id);

         return response()->json([
            'success' => 'table_deleted',
            'message' => 'Table Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deleteSubsetCheckbox($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteSubsetCheckbox($id);

         return response()->json([
            'success' => 'checkbox_deleted',
            'message' => 'Checkbox Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deleteSubsetCheckboxOption($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteSubsetCheckboxOption($id);

         return response()->json([
            'success' => 'checkbox_option_deleted',
            'message' => 'Checkbox Option Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deleteAreaDivisionTableContent(Request $request)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteAreaDivisionTableContent($request);

         return response()->json([
            'success' => 'content_deleted',
            'message' => 'Content Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deleteSubsetTable($id)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteSubsetTable($id);

         return response()->json([
            'success' => 'table_deleted',
            'message' => 'Table Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deleteSubsetTableContent(Request $request)
    {
        $AreaRepo = new AreaRepository(new Area);
        $data = $AreaRepo->deleteSubsetTableContent($request);

         return response()->json([
            'success' => 'content_deleted',
            'message' => 'Content Successfully Deleted',
            'data' => $data
        ],200);
    }



}
