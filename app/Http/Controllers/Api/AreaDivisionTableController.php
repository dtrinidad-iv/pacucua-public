<?php

namespace App\Http\Controllers\Api;


use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\AreaDivisionTables\Repositories\AreaDivisionTableRepository;
use App\Models\AreaDivisionTables\Repositories\AreaDivisionTableRepositoryInterface;
use App\Models\AreaDivisionTables\Requests\CreateAreaDivisionTableRequest;
use App\Models\AreaDivisionTables\Requests\UpdateAreaDivisionTableRequest;
use Illuminate\Http\Request;
use App\Models\AreaDivisionTables\AreaDivisionTable;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AreaDivisionTableController extends Controller
{

    use Authorizable;

    /**
     * @var AreaDivisionTableRepositoryInterface
     */
    private $areaDivisionTableRepo;

    /**
     * AreaDivisionTableController constructor.
     *
     * @param AreaDivisionTableRepositoryInterface $areaDivisionTableRepository
     */
    public function __construct(AreaDivisionTableRepositoryInterface $areaDivisionTableRepository)
    {
        $this->areaDivisionTableRepo = $areaDivisionTableRepository;
    }


    public function index(Request $request)
    {
      $areadivisiontables = $this->areaDivisionTableRepo->getAllAreaDivisionTables($request);

      return response()->json($areadivisiontables);
    }

    /**
     * @param CreateAreaDivisionTableRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAreaDivisionTableRequest $request)
    {


         $areaDivisionTableRepo = new AreaDivisionTableRepository(new AreaDivisionTable);
         $areadivisiontable = $areaDivisionTableRepo->createAreaDivisionTable($request->all());

         return response()->json([
             'success' => 'areadivisiontable_created',
             'message' => 'AreaDivisionTable Successfully Created',
             'data' => $areadivisiontable
         ],201);


    }

    public function show($id)
    {

      $areaDivisionTableRepo = new AreaDivisionTableRepository(new AreaDivisionTable);
      $areadivisiontable = $this->areaDivisionTableRepo->findAreaDivisionTableById($id);
      return response()->json($areadivisiontable);

    }

    public function update(UpdateAreaDivisionTableRequest $request, $id)
    {
          $areadivisiontable = $this->areaDivisionTableRepo->findAreaDivisionTableById($id);

          $update = new AreaDivisionTableRepository($areadivisiontable);
          $data = $update->updateAreaDivisionTable($request->all());

           return response()->json([
               'success' => 'areadivisiontable_updated',
               'message' => 'AreaDivisionTable Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $areadivisiontable = $this->areaDivisionTableRepo->findAreaDivisionTableById($id);

        $delete = new AreaDivisionTableRepository($areadivisiontable);
        $data = $delete->deleteAreaDivisionTable();

        return response()->json([
            'success' => 'areadivisiontable_deleted',
            'message' => 'AreaDivisionTable Successfully Deleted',
            'data' => $data
        ],200);


    }
}
