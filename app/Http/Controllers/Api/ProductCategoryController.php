<?php

namespace App\Http\Controllers\Api;


use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\ProductCategories\Repositories\ProductCategoryRepository;
use App\Models\ProductCategories\Repositories\ProductCategoryRepositoryInterface;
use App\Models\ProductCategories\Requests\CreateProductCategoryRequest;
use App\Models\ProductCategories\Requests\UpdateProductCategoryRequest;
use Illuminate\Http\Request;
use App\Models\ProductCategories\ProductCategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductCategoryController extends Controller
{

    use Authorizable;

    /**
     * @var ProductCategoryRepositoryInterface
     */
    private $productCategoryRepo;

    /**
     * CategoryController constructor.
     *
     * @param ProductCategoryRepositoryInterface $categoryRepository
     */
    public function __construct(ProductCategoryRepositoryInterface $productCategoryRepo)
    {
        $this->productCategoryRepo = $productCategoryRepo;
    }


    public function index(Request $request)
    {
      $categories = $this->productCategoryRepo->getAllProductCategories($request);

      return response()->json($categories);
    }

    /**
     * @param CreateProductCategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateProductCategoryRequest $request)
    {


         $productCategoryRepo = new ProductCategoryRepository(new ProductCategory);
         $category = $productCategoryRepo->createProductCategory($request->all());

         return response()->json([
             'success' => 'product_category_created',
             'message' => 'Product Category Successfully Created',
             'data' => $category
         ],201);


    }

    public function show($id)
    {

      $productCategoryRepo = new ProductCategoryRepository(new ProductCategory);
      $category = $productCategoryRepo->findProductCategoryById($id);
      return response()->json($category);

    }

    public function update(UpdateProductCategoryRequest $request, $id)
    {
          $category = $this->productCategoryRepo->findProductCategoryById($id);

          $update = new ProductCategoryRepository($category);
          $data = $update->updateProductCategory($request->all());

           return response()->json([
               'success' => 'product_category_updated',
               'message' => 'Product Category Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $category = $this->productCategoryRepo->findProductCategoryById($id);

        $delete = new ProductCategoryRepository($category);
        $data = $delete->deleteProductCategory();

        return response()->json([
            'success' => 'category_deleted',
            'message' => 'Category Successfully Deleted',
            'data' => $data
        ],200);


    }
}
