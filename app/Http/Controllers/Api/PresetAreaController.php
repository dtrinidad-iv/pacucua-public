<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\PresetAreas\Repositories\PresetAreaRepository;
use App\Models\PresetAreas\Repositories\PresetAreaRepositoryInterface;
use App\Models\PresetAreas\Requests\CreatePresetAreaRequest;
use App\Models\PresetAreas\Requests\UpdatePresetAreaRequest;
use Illuminate\Http\Request;
use App\Models\PresetAreas\PresetArea;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PresetAreaController extends Controller
{

    // use Authorizable;

    /**
     * @var PresetAreaRepositoryInterface
     */
    private $PresetAreaRepo;

    /**
     * PresetAreaController constructor.
     *
     * @param PresetAreaRepositoryInterface $presetAreaRepository
     */
    public function __construct(PresetAreaRepositoryInterface $PresetAreaRepo)
    {
        $this->PresetAreaRepo = $PresetAreaRepo;
    }


    public function index(Request $request)
    {
      $presetAreas = $this->PresetAreaRepo->getAllPresetAreas($request);

      return response()->json($presetAreas);
    }

    /**
     * @param CreatePresetAreaRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePresetAreaRequest $request)
    {


         $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
         $presetArea = $PresetAreaRepo->createPresetArea($request->all());

         return response()->json([
             'success' => 'presetArea_created',
             'message' => 'PresetArea Successfully Created',
             'data' => $presetArea
         ],201);


    }

    public function show($id)
    {

      $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
      $presetArea = $PresetAreaRepo->findPresetAreaById($id);
      return response()->json($presetArea);

    }

    public function update(UpdatePresetAreaRequest $request, $id)
    {   
          $presetArea = $this->PresetAreaRepo->findPresetAreaById($id);

          $update = new PresetAreaRepository($presetArea);
          $data = $update->updatePresetArea($request->all());

           return response()->json([
               'success' => 'presetArea_updated',
               'message' => 'PresetArea Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $presetArea = $this->PresetAreaRepo->findPresetAreaById($id);

        $delete = new PresetAreaRepository($presetArea);
        $data = $delete->deletePresetArea();

        return response()->json([
            'success' => 'presetArea_deleted',
            'message' => 'Category Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function getPresetAreasByProgram($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $presetArea = $PresetAreaRepo->getPresetAreasByProgram($id);

        return response()->json($presetArea);
    }

    public function deletePresetAreaDivision($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetAreaDivision($id);

        return response()->json([
            'success' => 'presetArea_division_deleted',
            'message' => 'PresetArea Division Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deletePresetDivisionSubset($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetDivisionSubset($id);

         return response()->json([
            'success' => 'presetArea_deleted',
            'message' => 'PresetArea Division Subset Successfully Deleted',
             'data' => $data
        ],200);
    }


    public function getPresetAreaDivision($id)
    {
      $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
      $presetAreaDivision = $PresetAreaRepo->getPresetAreaDivision($id);

      return response()->json($presetAreaDivision);
    }

    public function addPresetPresetArea(Request $request)
    {
         $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
         $presetArea = $PresetAreaRepo->addPresetPresetArea($request);

         return response()->json([
             'success' => 'presetArea_created',
             'message' => 'PresetArea Successfully Created',
             'data' => $presetArea
         ],201);

    }

    public function deletePresetAreaDivisionTable($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetAreaDivisionTable($id);

         return response()->json([
            'success' => 'table_deleted',
            'message' => 'Table Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deletePresetSubsetCheckbox($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetSubsetCheckbox($id);

         return response()->json([
            'success' => 'checkbox_deleted',
            'message' => 'Checkbox Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deletePresetSubsetCheckboxOption($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetSubsetCheckboxOption($id);

         return response()->json([
            'success' => 'checkbox_option_deleted',
            'message' => 'Checkbox Option Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deletePresetAreaDivisionTableContent(Request $request)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetAreaDivisionTableContent($request);

         return response()->json([
            'success' => 'content_deleted',
            'message' => 'Content Successfully Deleted',
            'data' => $data
        ],200);
    }

    public function deletePresetSubsetTable($id)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetSubsetTable($id);

         return response()->json([
            'success' => 'table_deleted',
            'message' => 'Table Successfully Deleted',
             'data' => $data
        ],200);
    }

    public function deletePresetSubsetTableContent(Request $request)
    {
        $PresetAreaRepo = new PresetAreaRepository(new PresetArea);
        $data = $PresetAreaRepo->deletePresetSubsetTableContent($request);

         return response()->json([
            'success' => 'content_deleted',
            'message' => 'Content Successfully Deleted',
            'data' => $data
        ],200);
    }



}
