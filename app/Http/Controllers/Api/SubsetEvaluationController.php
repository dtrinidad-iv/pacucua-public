<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Evaluations\Repositories\SubsetEvaluationRepository;
use App\Models\Evaluations\Repositories\SubsetEvaluationRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\Evaluations\SubsetEvaluation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubsetEvaluationController extends Controller
{
   // use Authorizable;

    /**
     * @var SubsetEvaluationRepositoryInterface
     */
    private $SubsetEvaluationRepo;

    /**
     * SubsetEvaluationController constructor.
     *
     * @param SubsetEvaluationRepositoryInterface $subset_evaluationRepository
     */
    public function __construct(SubsetEvaluationRepositoryInterface $SubsetEvaluationRepo)
    {
        $this->SubsetEvaluationRepo = $SubsetEvaluationRepo;
    }


    public function index(Request $request)
    {
      $subset_evaluations = $this->SubsetEvaluationRepo->getAllSubsetEvaluations($request);

      return response()->json($subset_evaluations);
    }

    /**
     * @param CreateSubsetEvaluationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSubsetEvaluationRequest $request)
    {


         $SubsetEvaluationRepo = new SubsetEvaluationRepository(new SubsetEvaluation);
         $subset_evaluation = $SubsetEvaluationRepo->createSubsetEvaluation($request->all());

         return response()->json([
             'success' => 'subset_evaluation_created',
             'message' => 'SubsetEvaluation Successfully Created',
             'data' => $subset_evaluation
         ],201);


    }

    public function show($id)
    {

      $SubsetEvaluationRepo = new SubsetEvaluationRepository(new SubsetEvaluation);
      $subset_evaluation = $SubsetEvaluationRepo->findSubsetEvaluationById($id);
      return response()->json($subset_evaluation);

    }

    public function update(UpdateSubsetEvaluationRequest $request, $id)
    {
          $subset_evaluation = $this->SubsetEvaluationRepo->findSubsetEvaluationById($id);

          $update = new SubsetEvaluationRepository($subset_evaluation);
          $data = $update->updateSubsetEvaluation($request->all());

           return response()->json([
               'success' => 'subset_evaluation_updated',
               'message' => 'SubsetEvaluation Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $subset_evaluation = $this->SubsetEvaluationRepo->findSubsetEvaluationById($id);

        $delete = new SubsetEvaluationRepository($subset_evaluation);
        $data = $delete->deleteSubsetEvaluation();

        return response()->json([
            'success' => 'subset_evaluation_deleted',
            'message' => 'SubsetEvaluation Successfully Deleted',
            'data' => $data
        ],200);


    }

    public function saveSubsetEvaluation(Request $request)
    {
      $subset_evaluationRepo = new SubsetEvaluationRepository(new SubsetEvaluation);
      $subset_evaluation = $subset_evaluationRepo->saveSubsetEvaluation($request->all());

      return response()->json($subset_evaluation);
    }
}
