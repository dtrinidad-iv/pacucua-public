<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Programs\Repositories\ProgramRepository;
use App\Models\Programs\Repositories\ProgramRepositoryInterface;
use App\Models\Programs\Requests\CreateProgramRequest;
use App\Models\Programs\Requests\UpdateProgramRequest;
use Illuminate\Http\Request;
use App\Models\Programs\Program;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProgramController extends Controller
{
   // use Authorizable;

    /**
     * @var ProgramRepositoryInterface
     */
    private $ProgramRepo;

    /**
     * ProgramController constructor.
     *
     * @param ProgramRepositoryInterface $programRepository
     */
    public function __construct(ProgramRepositoryInterface $ProgramRepo)
    {
        $this->ProgramRepo = $ProgramRepo;
    }


    public function index(Request $request)
    {
      $programs = $this->ProgramRepo->getAllPrograms($request);

      return response()->json($programs);
    }

    /**
     * @param CreateProgramRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateProgramRequest $request)
    {


         $ProgramRepo = new ProgramRepository(new Program);
         $program = $ProgramRepo->createProgram($request->all());

         return response()->json([
             'success' => 'product_program_created',
             'message' => 'Product Program Successfully Created',
             'data' => $program
         ],201);


    }

    public function show($id)
    {

      $ProgramRepo = new ProgramRepository(new Program);
      $program = $ProgramRepo->findProgramById($id);
      return response()->json($program);

    }

    public function update(UpdateProgramRequest $request, $id)
    {
          $program = $this->ProgramRepo->findProgramById($id);

          $update = new ProgramRepository($program);
          $data = $update->updateProgram($request->all());

           return response()->json([
               'success' => 'product_program_updated',
               'message' => 'Product Program Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $program = $this->ProgramRepo->findProgramById($id);

        $delete = new ProgramRepository($program);
        $data = $delete->deleteProgram();

        return response()->json([
            'success' => 'program_deleted',
            'message' => 'Program Successfully Deleted',
            'data' => $data
        ],200);

    }

    public function getProgramsByDepartment($id)
    {
        $ProgramRepo = new ProgramRepository(new Program);
        $program = $ProgramRepo->getProgramByDepartment($id);
 
        return response()->json($program);
    }
}
