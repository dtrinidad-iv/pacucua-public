<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Evaluations\Repositories\EvaluationRepository;
use App\Models\Evaluations\Repositories\EvaluationRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\Evaluations\Evaluation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EvaluationController extends Controller
{
   // use Authorizable;

    /**
     * @var EvaluationRepositoryInterface
     */
    private $EvaluationRepo;

    /**
     * EvaluationController constructor.
     *
     * @param EvaluationRepositoryInterface $evaluationRepository
     */
    public function __construct(EvaluationRepositoryInterface $EvaluationRepo)
    {
        $this->EvaluationRepo = $EvaluationRepo;
    }


    public function index(Request $request)
    {
      $evaluations = $this->EvaluationRepo->getAllEvaluations($request);

      return response()->json($evaluations);
    }

    /**
     * @param CreateEvaluationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateEvaluationRequest $request)
    {


         $EvaluationRepo = new EvaluationRepository(new Evaluation);
         $evaluation = $EvaluationRepo->createEvaluation($request->all());

         return response()->json([
             'success' => 'evaluation_created',
             'message' => 'Evaluation Successfully Created',
             'data' => $evaluation
         ],201);


    }

    public function show($id)
    {

      $EvaluationRepo = new EvaluationRepository(new Evaluation);
      $evaluation = $EvaluationRepo->findEvaluationById($id);
      return response()->json($evaluation);

    }

    public function update(UpdateEvaluationRequest $request, $id)
    {
          $evaluation = $this->EvaluationRepo->findEvaluationById($id);

          $update = new EvaluationRepository($evaluation);
          $data = $update->updateEvaluation($request->all());

           return response()->json([
               'success' => 'evaluation_updated',
               'message' => 'Evaluation Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $evaluation = $this->EvaluationRepo->findEvaluationById($id);

        $delete = new EvaluationRepository($evaluation);
        $data = $delete->deleteEvaluation();

        return response()->json([
            'success' => 'evaluation_deleted',
            'message' => 'Evaluation Successfully Deleted',
            'data' => $data
        ],200);


    }

    public function saveEvaluation(Request $request)
    {
      $evaluationRepo = new EvaluationRepository(new Evaluation);
      $evaluation = $evaluationRepo->saveEvaluation($request->all());

      return response()->json($evaluation);
    }
}
