<?php

namespace App\Http\Controllers\Api;


use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Evidences\Repositories\EvidenceRepository;
use App\Models\Evidences\Repositories\EvidenceRepositoryInterface;
use App\Models\Evidences\Requests\CreateEvidenceRequest;
use App\Models\Evidences\Requests\UpdateEvidenceRequest;
use Illuminate\Http\Request;
use App\Models\Evidences\Evidence;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EvidenceController extends Controller
{

  // /  use Authorizable;

    /**
     * @var EvidenceRepositoryInterface
     */
    private $evidenceRepo;

    /**
     * EvidenceController constructor.
     *
     * @param EvidenceRepositoryInterface $evidenceRepository
     */
    public function __construct(EvidenceRepositoryInterface $evidenceRepository)
    {
        $this->evidenceRepo = $evidenceRepository;
    }


    public function index(Request $request)
    {
      $evidences = $this->evidenceRepo->getAllEvidences($request);

      return response()->json($evidences);
    }

    /**
     * @param CreateEvidenceRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateEvidenceRequest $request)
    {


         $evidenceRepo = new EvidenceRepository(new Evidence);
         $evidence = $evidenceRepo->createEvidence($request);

         return response()->json([
             'success' => 'evidence_created',
             'message' => 'Evidence Successfully Created',
             'data' => $evidence
         ],201);


    }

    public function show($id)
    {

      $evidenceRepo = new EvidenceRepository(new Evidence);
      $evidence = $this->evidenceRepo->findEvidenceById($id);
      return response()->json($evidence);

    }

    public function update(UpdateEvidenceRequest $request, $id)
    {
          $evidence = $this->evidenceRepo->findEvidenceById($id);

          $update = new EvidenceRepository($evidence);
          $data = $update->updateEvidence($request->all());

           return response()->json([
               'success' => 'evidence_updated',
               'message' => 'Evidence Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $evidence = $this->evidenceRepo->findEvidenceById($id);

        $delete = new EvidenceRepository($evidence);
        $data = $delete->deleteEvidence();

        return response()->json([
            'success' => 'evidence_deleted',
            'message' => 'Evidence Successfully Deleted',
            'data' => $data
        ],200);


    }
}
