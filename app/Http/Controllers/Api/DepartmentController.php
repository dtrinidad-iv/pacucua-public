<?php

namespace App\Http\Controllers\Api;

use App\Models\Traits\Authorizable;
use App\Http\Controllers\Controller;
use App\Models\Departments\Repositories\DepartmentRepository;
use App\Models\Departments\Repositories\DepartmentRepositoryInterface;
use App\Models\Departments\Requests\CreateDepartmentRequest;
use App\Models\Departments\Requests\UpdateDepartmentRequest;
use Illuminate\Http\Request;
use App\Models\Departments\Department;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DepartmentController extends Controller
{
   // use Authorizable;

    /**
     * @var DepartmentRepositoryInterface
     */
    private $DepartmentRepo;

    /**
     * DepartmentController constructor.
     *
     * @param DepartmentRepositoryInterface $departmentRepository
     */
    public function __construct(DepartmentRepositoryInterface $DepartmentRepo)
    {
        $this->DepartmentRepo = $DepartmentRepo;
    }


    public function index(Request $request)
    {
      $departments = $this->DepartmentRepo->getAllDepartments($request);

      return response()->json($departments);
    }

    /**
     * @param CreateDepartmentRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDepartmentRequest $request)
    {


         $DepartmentRepo = new DepartmentRepository(new Department);
         $department = $DepartmentRepo->createDepartment($request->all());

         return response()->json([
             'success' => 'product_department_created',
             'message' => 'Product Department Successfully Created',
             'data' => $department
         ],201);


    }

    public function show($id)
    {

      $DepartmentRepo = new DepartmentRepository(new Department);
      $department = $DepartmentRepo->findDepartmentById($id);
      return response()->json($department);

    }

    public function update(UpdateDepartmentRequest $request, $id)
    {
          $department = $this->DepartmentRepo->findDepartmentById($id);

          $update = new DepartmentRepository($department);
          $data = $update->updateDepartment($request->all());

           return response()->json([
               'success' => 'product_department_updated',
               'message' => 'Product Department Successfully Updated',
               'data' => $data
           ],200);

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department = $this->DepartmentRepo->findDepartmentById($id);

        $delete = new DepartmentRepository($department);
        $data = $delete->deleteDepartment();

        return response()->json([
            'success' => 'department_deleted',
            'message' => 'Department Successfully Deleted',
            'data' => $data
        ],200);


    }
}
