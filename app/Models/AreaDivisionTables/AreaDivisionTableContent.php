<?php

namespace App\Models\AreaDivisionTables;

use Illuminate\Database\Eloquent\Model;

class AreaDivisionTableContent extends Model
{
    protected $table = 'area_division_table_contents';
    public $timestamps = false;
    protected $fillable = ['area_division_table_id','column','content','row'];

  
}
