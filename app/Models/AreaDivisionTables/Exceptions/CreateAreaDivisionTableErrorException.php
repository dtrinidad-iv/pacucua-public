<?php

namespace App\Models\AreaDivisionTables\Exceptions;

class CreateAreaDivisionTableErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_area_division_table_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
