<?php

namespace App\Models\AreaDivisionTables\Exceptions;

class UpdateAreaDivisionTableErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_area_division_table_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
