<?php

namespace App\Models\AreaDivisionTables\Exceptions;

class DeleteAreaDivisionTableErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_area_division_table_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
