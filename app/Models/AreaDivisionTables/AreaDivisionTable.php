<?php

namespace App\Models\AreaDivisionTables;

use Illuminate\Database\Eloquent\Model;

class AreaDivisionTable extends Model
{
    protected $table = 'area_division_tables';
    public $timestamps = true;
    protected $fillable = ['area_division_id','row_count','column_count','instruction'];

  	public function content()
    {
    	return $this->hasMany('App\Models\AreaDivisionTables\AreaDivisionTableContent','area_division_table_id','id');
    }

}
