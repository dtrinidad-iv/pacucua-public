<?php

namespace App\Models\AreaDivisionTables\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\AreaDivisionTables\AreaDivisionTable;
use App\Models\AreaDivisionTables\Exceptions\AreaDivisionTableNotFoundErrorException;
use App\Models\AreaDivisionTables\Exceptions\CreateAreaDivisionTableErrorException;
use App\Models\AreaDivisionTables\Exceptions\UpdateAreaDivisionTableErrorException;
use App\Models\AreaDivisionTables\Exceptions\DeleteAreaDivisionTableErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class AreaDivisionTableRepository extends BaseRepository implements AreaDivisionTableRepositoryInterface
{
    /**
     * AreaDivisionTableRepository constructor.
     *
     * @param AreaDivisionTable $areadivisiontable
     */
    public function __construct(AreaDivisionTable $areadivisiontable)
    {
        parent::__construct($areadivisiontable);
        $this->model = $areadivisiontable;
    }

    /**
     * @param array $data
     *
     * @return AreaDivisionTable
     * @throws CreateAreaDivisionTableErrorException
     */
    public function createAreaDivisionTable(array $data) : array
    {
        try {

           return $this->create($data);


        } catch (QueryException $e) {
            throw new CreateAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return AreaDivisionTable
     * @throws AreaDivisionTableNotFoundErrorException
     */
    public function findAreaDivisionTableById(int $id) : AreaDivisionTable
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new AreaDivisionTableNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateAreaDivisionTableErrorException
     */
    public function updateAreaDivisionTable(array $data) : bool
    {
        try {
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteAreaDivisionTable() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listAreaDivisionTables($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllAreaDivisionTables(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $areadivisiontables = $this->model->where('name', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $areadivisiontables = $this->model->paginate(5);
               return response()->json(compact('areadivisiontables'));
          }else{
            $areadivisiontables = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $areadivisiontables;
    }
}
