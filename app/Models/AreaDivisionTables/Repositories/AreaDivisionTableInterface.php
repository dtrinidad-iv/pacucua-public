<?php

namespace App\Models\AreaDivisionTables\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\AreaDivisionTables\AreaDivisionTable;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface AreaDivisionTableRepositoryInterface extends BaseRepositoryInterface
{
    public function createAreaDivisionTable(array $data);

    public function findAreaDivisionTableById(int $id) : AreaDivisionTable;

    public function updateAreaDivisionTable(array $data) : bool;

    public function deleteAreaDivisionTable() : bool;

    public function listAreaDivisionTables($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllAreaDivisionTables(Request $request);
}
