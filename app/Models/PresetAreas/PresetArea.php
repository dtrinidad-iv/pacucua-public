<?php

namespace App\Models\PresetAreas;

use Illuminate\Database\Eloquent\Model;

class PresetArea extends Model
{
    protected $table = 'preset_areas';
    public $timestamps = true;
    protected $fillable = ['code','title','weight_value','introduction'];

    public function areadivisions()
    {
         return $this->hasMany('App\Models\PresetAreas\PresetAreaDivision','area_id','id');
    }

}
