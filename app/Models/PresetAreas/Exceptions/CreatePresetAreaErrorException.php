<?php

namespace App\Models\PresetAreas\Exceptions;

class CreatePresetAreaErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_preset_area_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
