<?php

namespace App\Models\PresetAreas\Exceptions;

class UpdatePresetAreaErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_preset_area_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
