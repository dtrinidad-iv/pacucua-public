<?php

namespace App\Models\PresetAreas\Exceptions;

class PresetAreaNotFoundErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'preset_area_not_found',
              'message' => $this->getMessage()
          ],404);
    }
}
