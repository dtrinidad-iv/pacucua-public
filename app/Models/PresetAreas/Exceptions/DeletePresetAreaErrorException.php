<?php

namespace App\Models\PresetAreas\Exceptions;

class DeletePresetAreaErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_preset_area_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
