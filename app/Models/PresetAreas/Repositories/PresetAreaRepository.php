<?php

namespace App\Models\PresetAreas\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\PresetAreas\PresetArea;
use App\Models\PresetAreas\PresetAreaDivision;
use App\Models\PresetAreas\PresetAreaDivisionSubset;
use App\Models\PresetAreaDivisionTables\PresetAreaDivisionTable;
use App\Models\PresetAreaDivisionTables\PresetAreaDivisionTableContent;
use App\Models\PresetSubsetTables\PresetSubsetTable;
use App\Models\PresetSubsetTables\PresetTableContent;
use App\Models\PresetSubsetCheckboxes\PresetSubsetCheckbox;
use App\Models\PresetSubsetCheckboxes\PresetSubsetCheckboxOptions;
use App\Models\PresetAreas\Exceptions\PresetAreaNotFoundErrorException;
use App\Models\PresetAreas\Exceptions\CreatePresetAreaErrorException;
use App\Models\PresetAreas\Exceptions\UpdatePresetAreaErrorException;
use App\Models\PresetAreas\Exceptions\DeletePresetAreaErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class PresetAreaRepository extends BaseRepository implements PresetAreaRepositoryInterface
{
    /**
     * PresetAreaRepository constructor.
     *
     * @param PresetArea $area
     */
    public function __construct(PresetArea $area)
    {
        parent::__construct($area);
        $this->model = $area;
    }

    /**
     * @param array $data
     *
     * @return PresetArea
     * @throws CreatePresetAreaErrorException
     */
    public function createPresetArea(array $data)
    {
        try {

         \DB::transaction(function() use ($data)
          {
            $area_id =  \DB::table('preset_areas')->insertGetId([
                  "code" => $data['code'],
                  "title" => $data['title'],
                  "weight_value" => $data['weight_value'],
                  "introduction" => $data['introduction'],
                  "created_at" => date("Y-m-d H:i:s"),
                  "updated_at" => date("Y-m-d H:i:s")
                 ]);

            foreach ($data['areadivisions'] as $ad)
            {

                $area_division_id =  \DB::table('preset_area_divisions')->insertGetId([
                     "title" => $ad['title'],
                     "code" => $ad['code'],
                     "criteria" => $ad['criteria'],
                     "area_id" => $area_id,
                     "created_at" => date("Y-m-d H:i:s"),
                     "updated_at" => date("Y-m-d H:i:s")
                 ]);

                if($ad['tables'])
                {
                  foreach ($ad['tables'] as $table)
                      {
                        $table_id =\DB::table('preset_area_division_tables')->insertGetId([
                              "area_division_id" => $area_division_id,
                              "instruction" => $table['instruction'],
                              "row_count" => count($table['row']),
                              "column_count" => count($table['row'][0]['col']),
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                        foreach($table['row'] as $index_row => $row)
                        {
                          foreach($row['col'] as $index_col => $col)
                          {
                            if($col['content'] == null  || $content['content'] =='' || $content['content'] =='')
                              $isBlank = 1;
                            else
                              $isBlank = 0;

                              \DB::table('preset_area_division_table_contents')->insertGetId([
                                "table_id" => $table_id,
                                "content" => $col['content'],
                                "isBlank" => $isBlank,
                                "row" => $col['row'],
                                "column" => $index_col,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                          }
                        }
                         
                      }
                }

                // First Level Subset
                if($ad['subsets'])
                {
                  foreach ($ad['subsets'] as $scf)
                  {

                    $scf_id =\DB::table('preset_area_division_subsets')->insertGetId([
                              "description" => $scf['description'],
                              "code" => $scf['code'],
                              "area_division_id" => $area_division_id,
                              "parent_subset_id" => NULL,
                              "with_ratebox" =>$scf['with_ratebox'],
                              "with_checkbox" =>$scf['with_checkbox'],
                              "with_textarea" =>$scf['with_textarea'],
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                    if($scf['tables'])
                    {
                      foreach ($scf['tables'] as $table)
                      {
                        $table_id =\DB::table('preset_area_division_subset_tables')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $table['instruction'],
                              "row_count" => count($table['row']),
                              "column_count" => count($table['row'][0]['col']),
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                        foreach($table['row'] as $index_row => $row)
                        {
                          foreach($row['col'] as $index_col => $col)
                          {
                            if($col['content'] == null  || $content['content'] =='' || $content['content'] =='')
                              $isBlank = 1;
                            else
                              $isBlank = 0;

                              \DB::table('preset_table_contents')->insertGetId([
                                "subset_table_id" => $table_id,
                                "isBlank" => $isBlank,
                                "content" => $col['content'],
                                "row" => $col['row'],
                                "column" => $index_col,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                          }
                        }
                         
                      }
                    }

                    if($scf['checkboxes'])
                    {
                      foreach ($scf['checkboxes'] as $checkbox)
                      {
                        $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $checkbox['instruction']
                          ]);

                        foreach($checkbox['options'] as $option)
                        {
                          \DB::table('preset_subset_checkbox_options')->insert([
                              "checkbox_id" => $checkbox_id,
                              "option" => $option['option']
                          ]);
                        }
                         
                      }
                    }

                     // Second Level Subset
                    if($scf['subsets'])
                    {
                      foreach ($scf['subsets'] as $scs)
                      {

                        $scs_id =\DB::table('preset_area_division_subsets')->insertGetId([
                                  "description" => $scs['description'],
                                  "code" => $scs['code'],
                                  "area_division_id" => $area_division_id,
                                  "parent_subset_id" => $scf_id,
                                  "with_ratebox" =>$scs['with_ratebox'],
                                  "with_checkbox" =>$scs['with_checkbox'],
                                  "with_textarea" =>$scs['with_textarea'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                        if($scs['tables'])
                        {
                          foreach ($scs['tables'] as $table)
                          {
                            $table_id =\DB::table('preset_area_division_subset_tables')->insertGetId([
                                  "subset_id" => $scs_id,
                                  "instruction" => $table['instruction'],
                                  "row_count" => count($table['row']),
                                  "column_count" => count($table['row'][0]['col']),
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                            foreach($table['row'] as $index_row => $row)
                            {
                              foreach($row['col'] as $index_col => $col)
                              {
                                if($col['content'] == null  || $content['content'] =='' || $content['content'] =='')
                                  $isBlank = 1;
                                else
                                  $isBlank = 0;

                                  \DB::table('preset_table_contents')->insertGetId([
                                    "subset_table_id" => $table_id,
                                    "content" => $col['content'],
                                    "isBlank" => $isBlank,
                                    "row" => $col['row'],
                                    "column" => $index_col,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);
                              }
                            }
                             
                          }
                        }

                        if($scs['checkboxes'])
                        {
                          foreach ($scs['checkboxes'] as $checkbox)
                          {
                            $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                                  "subset_id" => $scs_id,
                                  "instruction" => $checkbox['instruction']
                              ]);

                            foreach($checkbox['options'] as $option)
                            {
                              \DB::table('preset_subset_checkbox_options')->insert([
                                  "checkbox_id" => $checkbox_id,
                                  "option" => $option['option']
                              ]);
                            }
                             
                          }
                        }

                         // Third Level Subset
                        if($scs['subsets'])
                        {
                          foreach ($scs['subsets'] as $sct)
                          {

                            $sct_id = \DB::table('preset_area_division_subsets')->insertGetId([
                                      "description" => $sct['description'],
                                      "code" => $sct['code'],
                                      "area_division_id" => $area_division_id,
                                      "parent_subset_id" => $scs_id,
                                      "with_ratebox" =>$sct['with_ratebox'],
                                      "with_checkbox" =>$sct['with_checkbox'],
                                      "with_textarea" =>$sct['with_textarea'],
                                      "created_at" => date("Y-m-d H:i:s"),
                                      "updated_at" => date("Y-m-d H:i:s")
                                  ]);

                              if($sct['tables'])
                              {
                                foreach ($sct['tables'] as $table)
                                {
                                  $table_id =\DB::table('preset_area_division_subset_tables')->insertGetId([
                                        "subset_id" => $sct_id,
                                        "instruction" => $table['instruction'],
                                        "row_count" => count($table['row']),
                                        "column_count" => count($table['row'][0]['col']),
                                        "created_at" => date("Y-m-d H:i:s"),
                                        "updated_at" => date("Y-m-d H:i:s")
                                    ]);

                                  foreach($table['row'] as $index_row => $row)
                                  {
                                    foreach($row['col'] as $index_col => $col)
                                    {
                                      if($col['content'] == null  || $content['content'] =='' || $content['content'] =='')
                                        $isBlank = 1;
                                      else
                                        $isBlank = 0;

                                        \DB::table('preset_table_contents')->insertGetId([
                                          "subset_table_id" => $table_id,
                                          "content" => $col['content'],
                                          "isBlank" => $isBlank,
                                          "row" => $col['row'],
                                          "column" => $index_col,
                                          "created_at" => date("Y-m-d H:i:s"),
                                          "updated_at" => date("Y-m-d H:i:s")
                                      ]);
                                    }
                                  }
                                   
                                }
                              }

                               if($sct['checkboxes'])
                                {
                                  foreach ($sct['checkboxes'] as $checkbox)
                                  {
                                    $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                                          "subset_id" => $sct_id,
                                          "instruction" => $checkbox['instruction']
                                      ]);

                                    foreach($checkbox['options'] as $option)
                                    {
                                      \DB::table('preset_subset_checkbox_options')->insert([
                                          "checkbox_id" => $checkbox_id,
                                          "option" => $option['option']
                                      ]);
                                    }
                                     
                                  }
                                }
                           }
                           
                        }  // End of Third Level


                       } // End of foreach
                       
                    } // End of Second Level


                   } // End of foreach

                } // End of First Level

            }// End of foreach

          });

        return $data;

        } catch (QueryException $e) {
            throw new CreatePresetAreaErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return PresetArea
     * @throws PresetAreaNotFoundErrorException
     */
    public function findPresetAreaById(int $id) : PresetArea
    {
        try {
            return $this->findOneOrFail($id)->load('areadivisions','areadivisions.subsets','areadivisions.tables','areadivisions.subsets.tables','areadivisions.subsets.checkboxes');
        } catch (ModelNotFoundException $e) {
            throw new PresetAreaNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdatePresetAreaErrorException
     */
    public function updatePresetArea(array $data):bool
    {
        try {

          \DB::transaction(function() use ($data)
          {
            foreach ($data['areadivisions'] as $areadivision) {
               if(array_key_exists('id', $areadivision))
               {
                  $areaDiv = PresetAreaDivision::find($areadivision['id']);
                  $areaDiv->update($areadivision);
                  $area_division_id = $areadivision['id'];

                }
                else
                {
                  $area_division_id =  \DB::table('preset_area_divisions')->insertGetId([
                       "title" => $areadivision['title'],
                       "code" => $areadivision['code'],
                       "criteria" => $areadivision['criteria'],
                       "area_id" => $data['id'],
                       "created_at" => date("Y-m-d H:i:s"),
                       "updated_at" => date("Y-m-d H:i:s")
                   ]);
                }
                if($areadivision['subsets'])
                {
                 foreach ($areadivision['subsets'] as $scf)
                 {
                   if(array_key_exists('id', $scf))
                   {
                      $divsubset = PresetAreaDivisionSubset::find($scf['id']);
                      $divsubset->update($scf);
                      $scf_id = $scf['id'];
                   }
                   else
                   {
                      $scf_id =\DB::table('preset_area_division_subsets')->insertGetId([
                                "description" => $scf['description'],
                                "code" => $scf['code'],
                                "area_division_id" => $area_division_id,
                                "parent_subset_id" => NULL,
                                "with_ratebox" =>$scf['with_ratebox'],
                                "with_checkbox" =>$scf['with_checkbox'],
                                "with_textarea" =>$scf['with_textarea'],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                   }


                  if($scf['subsets'])
                  {
                   foreach ($scf['subsets'] as $scs)
                   {
                     if(array_key_exists('id', $scs))
                     {
                        $scsSbset = PresetAreaDivisionSubset::find($scs['id']);
                        $scsSbset->update($scs);
                        $scs_id = $scs['id'];
                     }
                     else
                     {
                        $scs_id =\DB::table('preset_area_division_subsets')->insertGetId([
                                  "description" => $scs['description'],
                                  "code" => $scs['code'],
                                  "area_division_id" => $area_division_id,
                                  "parent_subset_id" => $scf_id,
                                  "with_ratebox" =>$scs['with_ratebox'],
                                  "with_checkbox" =>$scs['with_checkbox'],
                                  "with_textarea" =>$scs['with_textarea'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);


                     }

                    if($scs['subsets'])
                    {
                     foreach ($scs['subsets'] as $sct)
                     {
                       if(array_key_exists('id', $sct))
                       {
                          $sctSbset = PresetAreaDivisionSubset::find($sct['id']);
                          $sctSbset->update($sct);
                          $sct_id = $sct['id'];
                       }
                       else
                       {
                          $sct_id =\DB::table('preset_area_division_subsets')->insertGetId([
                                    "description" => $sct['description'],
                                    "code" => $sct['code'],
                                    "area_division_id" => $area_division_id,
                                    "parent_subset_id" => $scs_id,
                                    "with_ratebox" =>$sct['with_ratebox'],
                                    "with_checkbox" =>$sct['with_checkbox'],
                                    "with_textarea" =>$sct['with_textarea'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);

                       }


                      if($sct['checkboxes'])
                      {
                        foreach ($sct['checkboxes'] as $checkbox)
                        {
                          if(array_key_exists('id', $checkbox))
                          {
                              $t = PresetSubsetCheckbox::find($checkbox['id']);
                              $t->update($checkbox);
                              $checkbox_id = $checkbox['id'];
                          }
                          else
                          {
                            $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $checkbox['instruction']
                              ]);
                          }

                          if($checkbox['options'])
                          {
                            foreach($checkbox['options'] as $option)
                            {
                              if(array_key_exists('id', $option))
                              {
                                  $t = PresetSubsetCheckboxOptions::find($option['id']);
                                  $t->update($option);
                              }
                              else
                              {
                                  \DB::table('preset_subset_checkbox_options')->insert([
                                      "checkbox_id" => $checkbox_id,
                                      "option" => $option['option']
                                  ]);
                              }
                            }
                          }
                          
                           
                        }
                      }

                      if($sct['tables'])
                      {
                       
                        foreach ($sct['tables'] as $table)
                        {
                          if(array_key_exists('id', $table))
                          {
                              $t = PresetSubsetTable::find($table['id']);
                              $t->update($table);
                              $table_id = $table['id'];
                          }
                          else
                          {
                            $table_id =  \DB::table('preset_area_division_subset_tables')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $table['instruction'],
                                  "row_count" => $table['row_count'],
                                  "column_count" => $table['column_count'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                             ]);
                          }
                          foreach ($table['content'] as $content)
                          {
                            if($content['content'] == null  || $content['content'] =='')
                              $content['isBlank'] = 1;
                            else
                              $content['isBlank'] = 0;

                            if(array_key_exists('id', $content))
                            {
                                $c = PresetTableContent::find($content['id']);
                                $c->isBlank =  $content['isBlank'];
                                $c->update($content);
                            }
                            else
                            {
                               \DB::table('preset_table_contents')->insertGetId([
                                        "subset_table_id" => $table_id,
                                        "content" => $content['content'],
                                        "isBlank" => $content['isBlank'],
                                        "row" => $content['row'],
                                        "column" => $content['column'],
                                        "created_at" => date("Y-m-d H:i:s"),
                                        "updated_at" => date("Y-m-d H:i:s")
                                    ]);
                            }
                          }
                        }
                      }


                     }//end of foreach sct
                    } 

                    if($scs['checkboxes'])
                    {
                      foreach ($scs['checkboxes'] as $checkbox)
                      {
                        if(array_key_exists('id', $checkbox))
                        {
                            $t = PresetSubsetCheckbox::find($checkbox['id']);
                            $t->update($checkbox);
                            $checkbox_id = $checkbox['id'];
                        }
                        else
                        {
                          $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $checkbox['instruction']
                            ]);
                        }

                        if($checkbox['options'])
                        {
                          foreach($checkbox['options'] as $option)
                          {
                            if(array_key_exists('id', $option))
                            {
                                $t = PresetSubsetCheckboxOptions::find($option['id']);
                                $t->update($option);
                            }
                            else
                            {
                                \DB::table('preset_subset_checkbox_options')->insert([
                                    "checkbox_id" => $checkbox_id,
                                    "option" => $option['option']
                                ]);
                            }
                          }
                        }
                        
                         
                      }
                    }

                    if($scs['tables'])
                    {
                     
                      foreach ($scs['tables'] as $table)
                      {
                        if(array_key_exists('id', $table))
                        {
                            $t = PresetSubsetTable::find($table['id']);
                            $t->update($table);
                            $table_id = $table['id'];
                        }
                        else
                        {
                          $table_id =  \DB::table('preset_area_division_subset_tables')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $table['instruction'],
                                "row_count" => $table['row_count'],
                                "column_count" => $table['column_count'],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                           ]);
                        }
                        foreach ($table['content'] as $content)
                        {
                          if($content['content'] == null  || $content['content'] =='')
                              $content['isBlank'] = 1;
                            else
                              $content['isBlank'] = 0;

                          if(array_key_exists('id', $content))
                          {
                              $c = PresetTableContent::find($content['id']);
                              $c->isBlank =  $content['isBlank'];
                              $c->update($content);
                          }
                          else
                          {
                             \DB::table('preset_table_contents')->insertGetId([
                                      "subset_table_id" => $table_id,
                                      "content" => $content['content'],
                                      "isBlank" => $content['isBlank'],
                                      "row" => $content['row'],
                                      "column" => $content['column'],
                                      "created_at" => date("Y-m-d H:i:s"),
                                      "updated_at" => date("Y-m-d H:i:s")
                                  ]);
                          }
                        }
                      }
                    }

                   }//end of foreach scs
                  } 

                  if($scf['checkboxes'])
                  {
                    foreach ($scf['checkboxes'] as $checkbox)
                    {
                      if(array_key_exists('id', $checkbox))
                      {
                          $t = PresetSubsetCheckbox::find($checkbox['id']);
                          $t->update($checkbox);
                          $checkbox_id = $checkbox['id'];
                      }
                      else
                      {
                        $checkbox_id =\DB::table('preset_subset_checkboxes')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $checkbox['instruction']
                          ]);
                      }

                      if($checkbox['options'])
                      {
                        foreach($checkbox['options'] as $option)
                        {
                          if(array_key_exists('id', $option))
                          {
                              $t = PresetSubsetCheckboxOptions::find($option['id']);
                              $t->update($option);
                          }
                          else
                          {
                              \DB::table('preset_subset_checkbox_options')->insert([
                                  "checkbox_id" => $checkbox_id,
                                  "option" => $option['option']
                              ]);
                          }
                        }
                      }
                      
                       
                    }
                  }

                  if($scf['tables'])
                  {
                   
                    foreach ($scf['tables'] as $table)
                    {
                      if(array_key_exists('id', $table))
                      {
                          $t = PresetSubsetTable::find($table['id']);
                          $t->update($table);
                          $table_id = $table['id'];
                      }
                      else
                      {
                        $table_id =  \DB::table('preset_area_division_subset_tables')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $table['instruction'],
                              "row_count" => $table['row_count'],
                              "column_count" => $table['column_count'],
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                         ]);
                      }
                      foreach ($table['content'] as $content)
                      {
                        if($content['content'] == null  || $content['content'] =='' || $content['content'] =='')
                              $content['isBlank'] = 1;
                            else
                              $content['isBlank'] = 0;

                        if(array_key_exists('id', $content))
                        {
                            $c = PresetTableContent::find($content['id']);
                            $c->isBlank =  $content['isBlank'];
                            $c->update($content);
                        }
                        else
                        {
                           \DB::table('preset_table_contents')->insertGetId([
                                    "subset_table_id" => $table_id,
                                    "content" => $content['content'],
                                    "isBlank" => $content['isBlank'],
                                    "row" => $content['row'],
                                    "column" => $content['column'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);
                        }
                      }
                    }
                  }

                 }//end of foreach
                } 

                if($areadivision['tables'])
                {
                  foreach ($areadivision['tables'] as $table)
                  {
                    if(array_key_exists('id', $table))
                    {
                        $t = PresetAreaDivisionTable::find($table['id']);
                        $t->update($table);
                        $table_id = $table['id'];
                    }
                    else
                    {
                      $table_id =  \DB::table('preset_area_division_tables')->insertGetId([
                            "area_division_id" => $area_division_id,
                            "instruction" => $table['instruction'],
                            "row_count" => $table['row_count'],
                            "column_count" => $table['column_count'],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                       ]);
                    }
                    foreach ($table['content'] as $content)
                    {
                      if($content['content'] == null  || $content['content'] =='' || $content['content'] =='')
                              $content['isBlank'] = 1;
                            else
                              $content['isBlank'] = 0;

                      if(array_key_exists('id', $content))
                      {
                          $c = PresetAreaDivisionTableContent::find($content['id']);
                          $c->isBlank =  $content['isBlank'];
                          $c->update($content);
                      }
                      else
                      {
                         \DB::table('preset_area_division_table_contents')->insertGetId([
                                  "table_id" => $table_id,
                                  "content" => $content['content'],
                                  "isBlank" => $content['isBlank'],
                                  "row" => $content['row'],
                                  "column" => $content['column'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);
                      }
                    }
                  }
                }
             }
          });
          
          return true;

        } catch (QueryException $e) {
            throw new UpdatePresetAreaErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deletePresetArea() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeletePresetAreaErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listPresetAreas($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllPresetAreas(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->where('title', 'like', '%' . $r["filter"] . '%')
                  ->orWhere('code', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->paginate(5);
               return response()->json(compact('areas'));
          }else{
            $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $areas;
    }


    public function deletePresetAreaDivision($id) : bool
    {
       try{
           $areaDiv = PresetAreaDivision::find($id);
           return $areaDiv->delete();

        } catch (QueryException $e) {
            throw new DeletePresetAreaDivisionErrorException($e);
        }
    }

    public function deletePresetDivisionSubset($id) : bool
    {
       try{
           $subset = PresetAreaDivisionSubset::find($id);
           return $subset->delete();

        } catch (QueryException $e) {
            throw new DeletePresetAreaDivisionErrorException($e);
        }
    }

    public function deletePresetSubsetCheckbox($id) : bool
    {
         $checkbox = PresetSubsetCheckbox::find($id);
         return $checkbox->delete();
    }

    public function deletePresetSubsetCheckboxOption($id) : bool
    {
         $checkbox = PresetSubsetCheckboxOptions::find($id);
         return $checkbox->delete();
    }

    public function deletePresetAreaDivisionTableContent(Request $request) : bool
    {
      $table = PresetAreaDivisionTable::find($request['table_id']);
      
      if($request['type'] == 'column')
      {
        foreach($request['columns'] as $column)
        {
          $c = PresetAreaDivisionTableContent::find($column);
          $c->delete();
         
        }
       $contents = PresetAreaDivisionTableContent::where('table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['column'] != 0)
          {
            $content['column'] = $content['column'] - 1;
            $content->update();
          }
        }
        if( $table->column_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->column_count =  $table->column_count - 1;

        $table->update();   

      }
      else if($request['type'] == 'row')
      {
        foreach($request['rows'] as $row)
        {
          $content = PresetAreaDivisionTableContent::find($row);
          $content->delete();
        }

        $contents = PresetAreaDivisionTableContent::where('table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['row'] != 0)
          {
            $content['row'] = $content['row'] - 1;
            $content->update();
          }
        }

        if($table->row_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->row_count =  $table->row_count - 1;

        $table->update();

        foreach($contents as $content)
        {
          $content['row'] = $content['row'] - 1;
        }
      }
     
      if($table->column_count == 0 && $table->row_count == 0)
      {
         $table->delete();
      }
      return true;
        
    }

    public function deletePresetAreaDivisionTable($id) : bool
    {
         $table = PresetAreaDivisionTable::find($id);
         return $table->delete();
    }

    public function deletePresetSubsetTableContent(Request $request) : bool
    {
      $table = PresetSubsetTable::find($request['table_id']);
      
      if($request['type'] == 'column')
      {
        foreach($request['columns'] as $column)
        {
          $c = PresetTableContent::find($column);
          $c->delete();
         
        }
       $contents = PresetTableContent::where('subset_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['column'] != 0)
          {
            $content['column'] = $content['column'] - 1;
            $content->update();
          }
        }
        if( $table->column_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->column_count =  $table->column_count - 1;

        $table->update();   

      }
      else if($request['type'] == 'row')
      {
        foreach($request['rows'] as $row)
        {
          $content = PresetTableContent::find($row);
          $content->delete();
        }

        $contents = PresetTableContent::where('subset_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['row'] != 0)
          {
            $content['row'] = $content['row'] - 1;
            $content->update();
          }
        }

        if($table->row_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->row_count =  $table->row_count - 1;

        $table->update();

        foreach($contents as $content)
        {
          $content['row'] = $content['row'] - 1;
        }
      }
     
      if($table->column_count == 0 && $table->row_count == 0)
      {
         $table->delete();
      }
      return true;
        
    }

    public function deletePresetSubsetTable($id) : bool
    {
         $table = PresetSubsetTable::find($id);
         return $table->delete();
    }

}