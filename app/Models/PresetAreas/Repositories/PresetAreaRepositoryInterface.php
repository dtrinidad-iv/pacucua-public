<?php

namespace App\Models\PresetAreas\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\PresetAreas\PresetArea;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface PresetAreaRepositoryInterface extends BaseRepositoryInterface
{
    public function createPresetArea(array $data);

    public function findPresetAreaById(int $id) : PresetArea;

    public function updatePresetArea(array $data) : bool;

    public function deletePresetArea() : bool;

    public function listPresetAreas($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllPresetAreas(Request $request);
}
