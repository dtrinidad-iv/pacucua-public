<?php

namespace App\Models\PresetAreas;

use Illuminate\Database\Eloquent\Model;

class PresetAreaDivision extends Model
{
    protected $table = 'preset_area_divisions';
    public $timestamps = true;
    protected $fillable = ['code','title','area_id','criteria'];

    public function subsets()
    {
         return $this->hasMany('App\Models\PresetAreas\PresetAreaDivisionSubset','area_division_id','id')->where('parent_subset_id', NULL);
    }

    public function area()
    {
    	return $this->belongsTo('App\Models\PresetAreas\PresetArea','area_id','id');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\PresetAreaDivisionTables\PresetAreaDivisionTable','area_division_id','id')->with('content');
    }
}
