<?php

namespace App\Models\PresetAreas;

use Illuminate\Database\Eloquent\Model;

class PresetAreaDivisionSubset extends Model
{
    protected $table = 'preset_area_division_subsets';
    public $timestamps = true;
    protected $fillable = ['code','description','area_division_id','parent_subset_id','with_ratebox','with_checkbox','with_textarea'];
 
    protected $appends = ['subsets'];


    public function getSubsetsAttribute()
    {
    	return \App\Models\PresetAreas\PresetAreaDivisionSubset::where('parent_subset_id',$this->id)->get()->load('tables','checkboxes');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\PresetSubsetTables\PresetSubsetTable','subset_id','id')->with('content');
    }

    public function checkboxes()
    {
        return $this->hasMany('App\Models\PresetSubsetCheckboxes\PresetSubsetCheckbox','subset_id','id')->with('options');
    }

}
