<?php

namespace App\Models\Programs\Requests;

use App\Models\Base\BaseFormRequest;

class CreateProgramRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'unique:programs']

        ];
    }

    public function messages()
    {
        return [
           'title.required' => 'The title field is required.'
        ];
    }
}
