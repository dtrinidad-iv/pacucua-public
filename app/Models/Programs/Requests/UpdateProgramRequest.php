<?php

namespace App\Models\Programs\Requests;

use App\Models\Base\BaseFormRequest;

class UpdateProgramRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'title' => ['required', 'unique:programs,title,' . $this->id],
      ];
    }
}
