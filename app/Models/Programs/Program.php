<?php

namespace App\Models\Programs;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';
    public $timestamps = true;
    protected $fillable = ['title', 'department_id'];

    public function areas(){
        return $this->hasMany('App\Models\Areas\Area');
    }

    public function department(){
        return $this->belongsTo('App\Models\Departments\Department');
    }

}
