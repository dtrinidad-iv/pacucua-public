<?php

namespace App\Models\Programs\Exceptions;

class ProgramNotFoundErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'program_not_found',
              'message' => $this->getMessage()
          ],404);
    }
}
