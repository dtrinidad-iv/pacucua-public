<?php

namespace App\Models\Programs\Exceptions;

class DeleteProgramErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_program_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
