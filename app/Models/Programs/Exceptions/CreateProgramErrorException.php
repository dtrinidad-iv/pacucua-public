<?php

namespace App\Models\Programs\Exceptions;

class CreateProgramErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_program_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
