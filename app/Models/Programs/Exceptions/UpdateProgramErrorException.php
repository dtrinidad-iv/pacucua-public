<?php

namespace App\Models\Programs\Exceptions;

class UpdateProgramErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_program_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
