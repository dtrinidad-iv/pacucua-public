<?php

namespace App\Models\Programs\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Programs\Program;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface ProgramRepositoryInterface extends BaseRepositoryInterface
{
    public function createProgram(array $data): array;

    public function findProgramById(int $id) : Program;

    public function updateProgram(array $data) : bool;

    public function deleteProgram() : bool;

    public function listPrograms($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllPrograms(Request $request);

}
