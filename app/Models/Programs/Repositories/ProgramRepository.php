<?php

namespace App\Models\Programs\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Programs\Program;
use App\Models\Programs\Exceptions\ProgramNotFoundErrorException;
use App\Models\Programs\Exceptions\CreateProgramErrorException;
use App\Models\Programs\Exceptions\UpdateProgramErrorException;
use App\Models\Programs\Exceptions\DeleteProgramErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class ProgramRepository extends BaseRepository implements ProgramRepositoryInterface
{
    /**
     * ProgramRepository constructor.
     *
     * @param Program $program
     */
    public function __construct(Program $program)
    {
        parent::__construct($program);
        $this->model = $program;
    }

    /**
     * @param array $data
     *
     * @return Program
     * @throws CreateProgramErrorException
     */
    public function createProgram(array $data) : array
    {
        try {

          // return $this->create($data);

         \DB::transaction(function() use ($data)
          {
            $program_id =  \DB::table('programs')->insertGetId([
                   "title" => $data['title'],
                   "department_id" => $data['department_id'],
                   "created_at" => date("Y-m-d H:i:s"),
                   "updated_at" => date("Y-m-d H:i:s")
                 ]);

          });

        return $data;

        } catch (QueryException $e) {
            throw new CreateProgramErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Program
     * @throws ProgramNotFoundErrorException
     */
    public function findProgramById(int $id) : Program
    {
        try {
            return $this->findOneOrFail($id)->load('department');
        } catch (ModelNotFoundException $e) {
            throw new ProgramNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateProgramErrorException
     */
    public function updateProgram(array $data) : bool
    {
        try {
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateProgramErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteProgram() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteProgramErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listPrograms($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllPrograms(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $programs = $this->model->where('name', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $programs = $this->model->paginate(5);
               return response()->json(compact('programs'));
          }else{
            $programs = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $programs;
    }

    public function getProgramByDepartment($id)
    {
        try {
            $programs = $this->model->where('department_id' , $id)->paginate(5);
            return $programs;
        } catch (ModelNotFoundException $e) {
            throw new ProgramNotFoundErrorException($e);
        }
    }


}
