<?php

namespace App\Models\SubsetTables;

use Illuminate\Database\Eloquent\Model;

class TableContent extends Model
{
    protected $table = 'table_contents';
    public $timestamps = false;
    protected $fillable = ['area_division_subset_table_id','column','content','row'];

  
}
