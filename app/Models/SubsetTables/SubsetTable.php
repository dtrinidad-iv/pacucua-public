<?php

namespace App\Models\SubsetTables;

use Illuminate\Database\Eloquent\Model;

class SubsetTable extends Model
{
    protected $table = 'area_division_subset_tables';
    public $timestamps = true;
    protected $fillable = ['subset_id','row_count','column_count','instruction'];

  	public function content()
    {
    	return $this->hasMany('App\Models\SubsetTables\TableContent','area_division_subset_table_id','id');
    }

}
