<?php

namespace App\Models\Areas\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Areas\Area;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface AreaRepositoryInterface extends BaseRepositoryInterface
{
    public function createArea(array $data);

    public function findAreaById(int $id) : Area;

    public function updateArea(array $data) : bool;

    public function deleteArea() : bool;

    public function listAreas($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllAreas(Request $request);
}
