<?php

namespace App\Models\Areas\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Areas\Area;
use App\Models\Areas\AreaDivision;
use App\Models\Areas\AreaDivisionSubset;
use App\Models\PresetAreas\PresetArea;
use App\Models\AreaDivisionTables\AreaDivisionTable;
use App\Models\AreaDivisionTables\AreaDivisionTableContent;
use App\Models\SubsetTables\SubsetTable;
use App\Models\SubsetTables\TableContent;
use App\Models\SubsetCheckboxes\SubsetCheckbox;
use App\Models\SubsetCheckboxes\SubsetCheckboxOptions;
use App\Models\Areas\Exceptions\AreaNotFoundErrorException;
use App\Models\Areas\Exceptions\CreateAreaErrorException;
use App\Models\Areas\Exceptions\UpdateAreaErrorException;
use App\Models\Areas\Exceptions\DeleteAreaErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class AreaRepository extends BaseRepository implements AreaRepositoryInterface
{
    /**
     * AreaRepository constructor.
     *
     * @param Area $area
     */
    public function __construct(Area $area)
    {
        parent::__construct($area);
        $this->model = $area;
    }

    /**
     * @param array $data
     *
     * @return Area
     * @throws CreateAreaErrorException
     */
    public function createArea(array $data)
    {
        try {

         \DB::transaction(function() use ($data)
          {
            $area_id =  \DB::table('areas')->insertGetId([
                  "code" => $data['code'],
                  "program_id" => $data['program_id'],
                  "title" => $data['title'],
                  "weight_value" => $data['weight_value'],
                  "introduction" => $data['introduction'],
                  "created_at" => date("Y-m-d H:i:s"),
                  "updated_at" => date("Y-m-d H:i:s")
                 ]);

            foreach ($data['areadivisions'] as $ad)
            {

                $area_division_id =  \DB::table('area_divisions')->insertGetId([
                     "title" => $ad['title'],
                     "code" => $ad['code'],
                     "criteria" => $ad['criteria'],
                     "area_id" => $area_id,
                     "created_at" => date("Y-m-d H:i:s"),
                     "updated_at" => date("Y-m-d H:i:s")
                 ]);

                if($ad['tables'])
                {
                  foreach ($ad['tables'] as $table)
                      {
                        $table_id =\DB::table('area_division_tables')->insertGetId([
                              "area_division_id" => $area_division_id,
                              "instruction" => $table['instruction'],
                              "row_count" => count($table['row']),
                              "column_count" => count($table['row'][0]['col']),
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                        foreach($table['row'] as $index_row => $row)
                        {
                          foreach($row['col'] as $index_col => $col)
                          {
                            if($col['content'] == null)
                              $isBlank = 1;
                            else
                              $isBlank = 0;

                              \DB::table('area_division_table_contents')->insertGetId([
                                "area_division_table_id" => $table_id,
                                "content" => $col['content'],
                                "isBlank" =>$isBlank,
                                "row" => $col['row'],
                                "column" => $index_col,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                          }
                        }
                         
                      }
                }

                // First Level Subset
                if($ad['subsets'])
                {
                  foreach ($ad['subsets'] as $scf)
                  {

                    $scf_id =\DB::table('area_division_subsets')->insertGetId([
                              "description" => $scf['description'],
                              "code" => $scf['code'],
                              "area_division_id" => $area_division_id,
                              "parent_subset_id" => NULL,
                              "with_ratebox" =>$scf['with_ratebox'],
                              "with_checkbox" =>$scf['with_checkbox'],
                              "with_textarea" =>$scf['with_textarea'],
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                    \DB::table('subset_evaluations')->insert([
                              "subset_id" => $scf_id,
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                    if($scf['tables'])
                    {
                      foreach ($scf['tables'] as $table)
                      {
                        $table_id =\DB::table('area_division_subset_tables')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $table['instruction'],
                              "row_count" => count($table['row']),
                              "column_count" => count($table['row'][0]['col']),
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                        foreach($table['row'] as $index_row => $row)
                        {
                          foreach($row['col'] as $index_col => $col)
                          {
                            if($col['content'] == null)
                              $isBlank = 1;
                            else
                              $isBlank = 0;

                              \DB::table('table_contents')->insertGetId([
                                "area_division_subset_table_id" => $table_id,
                                "content" => $col['content'],
                                "isBlank" => $isBlank,
                                "row" => $col['row'],
                                "column" => $index_col,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                          }
                        }
                         
                      }
                    }

                    if($scf['checkboxes'])
                    {
                      foreach ($scf['checkboxes'] as $checkbox)
                      {
                        $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $checkbox['instruction']
                          ]);

                        foreach($checkbox['options'] as $option)
                        {
                          \DB::table('subset_checkbox_options')->insert([
                              "checkbox_id" => $checkbox_id,
                              "option" => $option['option']
                          ]);
                        }
                         
                      }
                    }

                     // Second Level Subset
                    if($scf['subsets'])
                    {
                      foreach ($scf['subsets'] as $scs)
                      {

                        $scs_id =\DB::table('area_division_subsets')->insertGetId([
                                  "description" => $scs['description'],
                                  "code" => $scs['code'],
                                  "area_division_id" => $area_division_id,
                                  "parent_subset_id" => $scf_id,
                                  "with_ratebox" =>$scs['with_ratebox'],
                                  "with_checkbox" =>$scs['with_checkbox'],
                                  "with_textarea" =>$scs['with_textarea'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                        \DB::table('subset_evaluations')->insert([
                              "subset_id" => $scs_id,
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                          ]);

                        if($scs['tables'])
                        {
                          foreach ($scs['tables'] as $table)
                          {
                            $table_id =\DB::table('area_division_subset_tables')->insertGetId([
                                  "subset_id" => $scs_id,
                                  "instruction" => $table['instruction'],
                                  "row_count" => count($table['row']),
                                  "column_count" => count($table['row'][0]['col']),
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                            foreach($table['row'] as $index_row => $row)
                            {
                              foreach($row['col'] as $index_col => $col)
                              {

                                if($col['content'] == null)
                                  $isBlank = 1;
                                else
                                  $isBlank = 0;
                                

                                \DB::table('table_contents')->insertGetId([
                                    "area_division_subset_table_id" => $table_id,
                                    "content" => $col['content'],
                                    "row" => $col['row'],
                                    "isBlank" =>$isBlank,
                                    "column" => $index_col,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);
                              }
                            }
                             
                          }
                        }

                        if($scs['checkboxes'])
                        {
                          foreach ($scs['checkboxes'] as $checkbox)
                          {
                            $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                  "subset_id" => $scs_id,
                                  "instruction" => $checkbox['instruction']
                              ]);

                            foreach($checkbox['options'] as $option)
                            {
                              \DB::table('subset_checkbox_options')->insert([
                                  "checkbox_id" => $checkbox_id,
                                  "option" => $option['option']
                              ]);
                            }
                             
                          }
                        }

                         // Third Level Subset
                        if($scs['subsets'])
                        {
                          foreach ($scs['subsets'] as $sct)
                          {

                            $sct_id = \DB::table('area_division_subsets')->insertGetId([
                                      "description" => $sct['description'],
                                      "code" => $sct['code'],
                                      "area_division_id" => $area_division_id,
                                      "parent_subset_id" => $scs_id,
                                      "with_ratebox" =>$sct['with_ratebox'],
                                      "with_checkbox" =>$sct['with_checkbox'],
                                      "with_textarea" =>$sct['with_textarea'],
                                      "created_at" => date("Y-m-d H:i:s"),
                                      "updated_at" => date("Y-m-d H:i:s")
                                  ]);

                              \DB::table('subset_evaluations')->insert([
                                "subset_id" => $sct_id,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                              ]);

                              if($sct['tables'])
                              {
                                foreach ($sct['tables'] as $table)
                                {
                                  $table_id =\DB::table('area_division_subset_tables')->insertGetId([
                                        "subset_id" => $sct_id,
                                        "instruction" => $table['instruction'],
                                        "row_count" => count($table['row']),
                                        "column_count" => count($table['row'][0]['col']),
                                        "created_at" => date("Y-m-d H:i:s"),
                                        "updated_at" => date("Y-m-d H:i:s")
                                    ]);

                                  foreach($table['row'] as $index_row => $row)
                                  {
                                    foreach($row['col'] as $index_col => $col)
                                    {

                                        if($col['content'] == null)
                                          $isBlank = 1;
                                        else
                                          $isBlank = 0;
                                
                                        \DB::table('table_contents')->insertGetId([
                                          "area_division_subset_table_id" => $table_id,
                                          "content" => $col['content'],
                                          "isBlank" => $isBlank,
                                          "row" => $col['row'],
                                          "column" => $index_col,
                                          "created_at" => date("Y-m-d H:i:s"),
                                          "updated_at" => date("Y-m-d H:i:s")
                                      ]);
                                    }
                                  }
                                   
                                }
                              }

                               if($sct['checkboxes'])
                                {
                                  foreach ($sct['checkboxes'] as $checkbox)
                                  {
                                    $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                          "subset_id" => $sct_id,
                                          "instruction" => $checkbox['instruction']
                                      ]);

                                    foreach($checkbox['options'] as $option)
                                    {
                                      \DB::table('subset_checkbox_options')->insert([
                                          "checkbox_id" => $checkbox_id,
                                          "option" => $option['option']
                                      ]);
                                    }
                                     
                                  }
                                }
                           }
                           
                        }  // End of Third Level


                       } // End of foreach
                       
                    } // End of Second Level


                   } // End of foreach

                } // End of First Level

            }// End of foreach

          });

        return $data;

        } catch (QueryException $e) {
            throw new CreateAreaErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Area
     * @throws AreaNotFoundErrorException
     */
    public function findAreaById(int $id) : Area
    {
        try {
            return $this->findOneOrFail($id)->load('program','program.department','areadivisions','areadivisions.subsets','areadivisions.tables','areadivisions.subsets.tables','areadivisions.subsets.evaluation','areadivisions.subsets.evidences','areadivisions.subsets.checkboxes','areadivisions.evaluation');
        } catch (ModelNotFoundException $e) {
            throw new AreaNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateAreaErrorException
     */
    public function updateArea(array $data):bool
    {
        try {

          \DB::transaction(function() use ($data)
          {
            foreach ($data['areadivisions'] as $areadivision) {
               if(array_key_exists('id', $areadivision))
               {
                  $areaDiv = AreaDivision::find($areadivision['id']);
                  $areaDiv->update($areadivision);
                  $area_division_id = $areadivision['id'];

                }
                else
                {
                  $area_division_id =  \DB::table('area_divisions')->insertGetId([
                       "title" => $areadivision['title'],
                       "code" => $areadivision['code'],
                       "criteria" => $areadivision['criteria'],
                       "area_id" => $data['id'],
                       "created_at" => date("Y-m-d H:i:s"),
                       "updated_at" => date("Y-m-d H:i:s")
                   ]);
                }
                if($areadivision['subsets'])
                {
                 foreach ($areadivision['subsets'] as $scf)
                 {
                   if(array_key_exists('id', $scf))
                   {
                      $divsubset = AreaDivisionSubset::find($scf['id']);
                      $divsubset->update($scf);
                      $scf_id = $scf['id'];
                   }
                   else
                   {
                      $scf_id =\DB::table('area_division_subsets')->insertGetId([
                                "description" => $scf['description'],
                                "code" => $scf['code'],
                                "area_division_id" => $area_division_id,
                                "parent_subset_id" => NULL,
                                "with_ratebox" =>$scf['with_ratebox'],
                                "with_checkbox" =>$scf['with_checkbox'],
                                "with_textarea" =>$scf['with_textarea'],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);

                      \DB::table('subset_evaluations')->insert([
                                "subset_id" => $scf_id,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ]);
                   }


                  if($scf['subsets'])
                  {
                   foreach ($scf['subsets'] as $scs)
                   {
                     if(array_key_exists('id', $scs))
                     {
                        $scsSbset = AreaDivisionSubset::find($scs['id']);
                        $scsSbset->update($scs);
                     }
                     else
                     {
                        $scs_id =\DB::table('area_division_subsets')->insertGetId([
                                  "description" => $scs['description'],
                                  "code" => $scs['code'],
                                  "area_division_id" => $area_division_id,
                                  "parent_subset_id" => $scf_id,
                                  "with_ratebox" =>$scs['with_ratebox'],
                                  "with_checkbox" =>$scs['with_checkbox'],
                                  "with_textarea" =>$scs['with_textarea'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                        \DB::table('subset_evaluations')->insert([
                                  "subset_id" => $scs_id,
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                     }

                    if($scs['subsets'])
                    {
                     foreach ($scs['subsets'] as $sct)
                     {
                       if(array_key_exists('id', $sct))
                       {
                          $sctSbset = AreaDivisionSubset::find($sct['id']);
                          $sctSbset->update($sct);
                       }
                       else
                       {
                          $sct_id =\DB::table('area_division_subsets')->insertGetId([
                                    "description" => $sct['description'],
                                    "code" => $sct['code'],
                                    "area_division_id" => $area_division_id,
                                    "parent_subset_id" => $scs_id,
                                    "with_ratebox" =>$sct['with_ratebox'],
                                    "with_checkbox" =>$sct['with_checkbox'],
                                    "with_textarea" =>$sct['with_textarea'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);

                          \DB::table('subset_evaluations')->insert([
                                    "subset_id" => $sct_id,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);

                       }


                      if($sct['checkboxes'])
                      {
                        foreach ($sct['checkboxes'] as $checkbox)
                        {
                          if(array_key_exists('id', $checkbox))
                          {
                              $t = SubsetCheckbox::find($checkbox['id']);
                              $t->update($checkbox);
                              $checkbox_id = $checkbox['id'];
                          }
                          else
                          {
                            $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $checkbox['instruction']
                              ]);
                          }

                          if($checkbox['options'])
                          {
                            foreach($checkbox['options'] as $option)
                            {
                              if(array_key_exists('id', $option))
                              {
                                  $t = SubsetCheckboxOptions::find($option['id']);
                                  $t->update($option);
                              }
                              else
                              {
                                  \DB::table('subset_checkbox_options')->insert([
                                      "checkbox_id" => $checkbox_id,
                                      "option" => $option['option']
                                  ]);
                              }
                            }
                          }
                          
                           
                        }
                      }

                      if($sct['tables'])
                      {
                       
                        foreach ($sct['tables'] as $table)
                        {
                          if(array_key_exists('id', $table))
                          {
                              $t = SubsetTable::find($table['id']);
                              $t->update($table);
                              $table_id = $table['id'];
                          }
                          else
                          {
                            $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $table['instruction'],
                                  "row_count" => $table['row_count'],
                                  "column_count" => $table['column_count'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                             ]);
                          }
                          foreach ($table['content'] as $content)
                          {

                            if($content['content'] == null)
                              $content['isBlank'] = 1;
                            else
                               $content['isBlank'] = 0;

                            if(array_key_exists('id', $content))
                            {                                
                                $c = TableContent::find($content['id']);
                                $c->isBlank =  $content['isBlank'];
                                $c->update($content);
                            }
                            else
                            {

                               \DB::table('table_contents')->insertGetId([
                                        "area_division_subset_table_id" => $table_id,
                                        "content" => $content['content'],
                                        "isBlank" => $content['isBlank'],
                                        "row" => $content['row'],
                                        "column" => $content['column'],
                                        "created_at" => date("Y-m-d H:i:s"),
                                        "updated_at" => date("Y-m-d H:i:s")
                                    ]);
                            }
                          }
                        }
                      }


                     }//end of foreach sct
                    } 

                    if($scs['checkboxes'])
                    {
                      foreach ($scs['checkboxes'] as $checkbox)
                      {
                        if(array_key_exists('id', $checkbox))
                        {
                            $t = SubsetCheckbox::find($checkbox['id']);
                            $t->update($checkbox);
                            $checkbox_id = $checkbox['id'];
                        }
                        else
                        {
                          $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $checkbox['instruction']
                            ]);
                        }

                        if($checkbox['options'])
                        {
                          foreach($checkbox['options'] as $option)
                          {
                            if(array_key_exists('id', $option))
                            {
                                $t = SubsetCheckboxOptions::find($option['id']);
                                $t->update($option);
                            }
                            else
                            {
                                \DB::table('subset_checkbox_options')->insert([
                                    "checkbox_id" => $checkbox_id,
                                    "option" => $option['option']
                                ]);
                            }
                          }
                        }
                        
                         
                      }
                    }

                    if($scs['tables'])
                    {
                     
                      foreach ($scs['tables'] as $table)
                      {
                        if(array_key_exists('id', $table))
                        {
                            $t = SubsetTable::find($table['id']);
                            $t->update($table);
                            $table_id = $table['id'];
                        }
                        else
                        {
                          $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $table['instruction'],
                                "row_count" => $table['row_count'],
                                "column_count" => $table['column_count'],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                           ]);
                        }
                        foreach ($table['content'] as $content)
                        {

                            if($content['content'] == null)
                              $content['isBlank'] = 1;
                            else
                               $content['isBlank'] = 0;

                          if(array_key_exists('id', $content))
                          {
                              $c = TableContent::find($content['id']);
                              $c->isBlank =  $content['isBlank'];
                              $c->update($content);
                          }
                          else
                          {
                             \DB::table('table_contents')->insertGetId([
                                      "area_division_subset_table_id" => $table_id,
                                      "content" => $content['content'],
                                      "isBlank" =>  $content['isBlank'],
                                      "row" => $content['row'],
                                      "column" => $content['column'],
                                      "created_at" => date("Y-m-d H:i:s"),
                                      "updated_at" => date("Y-m-d H:i:s")
                                  ]);
                          }
                        }
                      }
                    }

                   }//end of foreach scs
                  } 

                  if($scf['checkboxes'])
                  {
                    foreach ($scf['checkboxes'] as $checkbox)
                    {
                      if(array_key_exists('id', $checkbox))
                      {
                          $t = SubsetCheckbox::find($checkbox['id']);
                          $t->update($checkbox);
                          $checkbox_id = $checkbox['id'];
                      }
                      else
                      {
                        $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $checkbox['instruction']
                          ]);
                      }

                      if($checkbox['options'])
                      {
                        foreach($checkbox['options'] as $option)
                        {
                          if(array_key_exists('id', $option))
                          {
                              $t = SubsetCheckboxOptions::find($option['id']);
                              $t->update($option);
                          }
                          else
                          {
                              \DB::table('subset_checkbox_options')->insert([
                                  "checkbox_id" => $checkbox_id,
                                  "option" => $option['option']
                              ]);
                          }
                        }
                      }
                      
                       
                    }
                  }

                  if($scf['tables'])
                  {
                   
                    foreach ($scf['tables'] as $table)
                    {
                      if(array_key_exists('id', $table))
                      {
                          $t = SubsetTable::find($table['id']);
                          $t->update($table);
                          $table_id = $table['id'];
                      }
                      else
                      {
                        $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $table['instruction'],
                              "row_count" => $table['row_count'],
                              "column_count" => $table['column_count'],
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                         ]);
                      }
                      foreach ($table['content'] as $content)
                      {
                        if($content['content'] == null)
                          $content['isBlank'] = 1;
                        else
                           $content['isBlank'] = 0;

                        if(array_key_exists('id', $content))
                        {
                            $c = TableContent::find($content['id']);
                            $c->isBlank =  $content['isBlank'];
                            $c->update($content);
                        }
                        else
                        {
                           \DB::table('table_contents')->insertGetId([
                                    "area_division_subset_table_id" => $table_id,
                                    "content" => $content['content'],
                                    "isBlank" =>  $content['isBlank'],
                                    "row" => $content['row'],
                                    "column" => $content['column'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);
                        }
                      }
                    }
                  }

                 }//end of foreach
                } 

                if($areadivision['tables'])
                {
                  foreach ($areadivision['tables'] as $table)
                  {
                    if(array_key_exists('id', $table))
                    {
                        $t = AreaDivisionTable::find($table['id']);
                        $t->update($table);
                        $table_id = $table['id'];
                    }
                    else
                    {
                      $table_id =  \DB::table('area_division_tables')->insertGetId([
                            "area_division_id" => $area_division_id,
                            "instruction" => $table['instruction'],
                            "row_count" => $table['row_count'],
                            "column_count" => $table['column_count'],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                       ]);
                    }
                    foreach ($table['content'] as $content)
                    {
                      if($content['content'] == null)
                          $content['isBlank'] = 1;
                      else
                         $content['isBlank'] = 0;

                      if(array_key_exists('id', $content))
                      {
                          $c = AreaDivisionTableContent::find($content['id']);
                          $c->isBlank =  $content['isBlank'];
                          $c->update($content);
                      }
                      else
                      {
                         \DB::table('area_division_table_contents')->insertGetId([
                                  "area_division_table_id" => $table_id,
                                  "content" => $content['content'],
                                  "isBlank" =>  $content['isBlank'],
                                  "row" => $content['row'],
                                  "column" => $content['column'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);
                      }
                    }
                  }
                }
             }
          });
          
          return true;

        } catch (QueryException $e) {
            throw new UpdateAreaErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteArea() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteAreaErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listAreas($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllAreas(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->where('title', 'like', '%' . $r["filter"] . '%')
                  ->orWhere('code', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->paginate(5);
               return response()->json(compact('areas'));
          }else{
            $areas = $this->model->with(['areadivisions','areadivisions.subsets'])->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $areas;
    }

    public function getAreasByProgram($id)
    {
        try {
            $areas = $this->model->where('program_id' , $id)->with('program','program.department','areadivisions','areadivisions.subsets', 'areadivisions.subsets.evaluation')->paginate(5);
            return $areas;
        } catch (ModelNotFoundException $e) {
            throw new ProgramNotFoundErrorException($e);
        }
    }


    public function deleteAreaDivision($id) : bool
    {
       try{
           $areaDiv = AreaDivision::find($id);
           return $areaDiv->delete();

        } catch (QueryException $e) {
            throw new DeleteAreaDivisionErrorException($e);
        }
    }

    public function deleteDivisionSubset($id) : bool
    {
       try{
           $subset = AreaDivisionSubset::find($id);
           return $subset->delete();

        } catch (QueryException $e) {
            throw new DeleteAreaDivisionErrorException($e);
        }
    }

    public function getAreaDivision($id)
    {
      $areaDivision =  AreaDivision::where('id' , $id)->with('area','tables','area.program','area.program.department','subsets.tables','subsets.evaluation','subsets.evidences')->first();
      return $areaDivision;
    }

    public function addPresetArea(Request $request)
    {
       $data =  PresetArea::where('id', $request['area_id'])->with('areadivisions','areadivisions.subsets','areadivisions.tables','areadivisions.subsets.tables','areadivisions.subsets.checkboxes')->first();
        $data['program_id'] =  $request['program_id'];


       \DB::transaction(function() use ($data)
          {
             $area_id =  \DB::table('areas')->insertGetId([
                  "code" => $data['code'],
                  "program_id" => $data['program_id'],
                  "title" => $data['title'],
                  "weight_value" => $data['weight_value'],
                  "introduction" => $data['introduction'],
                  "created_at" => date("Y-m-d H:i:s"),
                  "updated_at" => date("Y-m-d H:i:s")
                 ]);
            foreach ($data['areadivisions'] as $areadivision) {
               
                $area_division_id =  \DB::table('area_divisions')->insertGetId([
                     "title" => $areadivision['title'],
                     "code" => $areadivision['code'],
                     "criteria" => $areadivision['criteria'],
                     "area_id" => $area_id,
                     "created_at" => date("Y-m-d H:i:s"),
                     "updated_at" => date("Y-m-d H:i:s")
                 ]);
                
                if($areadivision['subsets'])
                {
                 foreach ($areadivision['subsets'] as $scf)
                 {
                  
                  $scf_id =\DB::table('area_division_subsets')->insertGetId([
                            "description" => $scf['description'],
                            "code" => $scf['code'],
                            "area_division_id" => $area_division_id,
                            "parent_subset_id" => NULL,
                            "with_ratebox" =>$scf['with_ratebox'],
                            "with_checkbox" =>$scf['with_checkbox'],
                            "with_textarea" =>$scf['with_textarea'],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ]);

                  \DB::table('subset_evaluations')->insert([
                            "subset_id" => $scf_id,
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ]);
                   

                  if($scf['subsets'])
                  {
                   foreach ($scf['subsets'] as $scs)
                   {
                     
                        $scs_id =\DB::table('area_division_subsets')->insertGetId([
                                  "description" => $scs['description'],
                                  "code" => $scs['code'],
                                  "area_division_id" => $area_division_id,
                                  "parent_subset_id" => $scf_id,
                                  "with_ratebox" =>$scs['with_ratebox'],
                                  "with_checkbox" =>$scs['with_checkbox'],
                                  "with_textarea" =>$scs['with_textarea'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);

                        \DB::table('subset_evaluations')->insert([
                                  "subset_id" => $scs_id,
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);


                    if($scs['subsets'])
                    {
                     foreach ($scs['subsets'] as $sct)
                     {
                       $sct_id =\DB::table('area_division_subsets')->insertGetId([
                                    "description" => $sct['description'],
                                    "code" => $sct['code'],
                                    "area_division_id" => $area_division_id,
                                    "parent_subset_id" => $scs_id,
                                    "with_ratebox" =>$sct['with_ratebox'],
                                    "with_checkbox" =>$sct['with_checkbox'],
                                    "with_textarea" =>$sct['with_textarea'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);

                          \DB::table('subset_evaluations')->insert([
                                    "subset_id" => $sct_id,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ]);

                      if($sct['checkboxes'])
                      {
                        foreach ($sct['checkboxes'] as $checkbox)
                        {
                          $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $checkbox['instruction']
                              ]);

                          if($checkbox['options'])
                          {
                            foreach($checkbox['options'] as $option)
                            {
                              \DB::table('subset_checkbox_options')->insert([
                                      "checkbox_id" => $checkbox_id,
                                      "option" => $option['option']
                                  ]);
                            }
                          }
                          
                           
                        }
                      }

                      if($sct['tables'])
                      {
                       
                        foreach ($sct['tables'] as $table)
                        {
                          $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                                  "subset_id" => $sct_id,
                                  "instruction" => $table['instruction'],
                                  "row_count" => $table['row_count'],
                                  "column_count" => $table['column_count'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                             ]);
                          
                          foreach ($table['content'] as $content)
                          {
                            if($content['content'] == null)
                              $content['isBlank'] = 1;
                            else
                               $content['isBlank'] = 0;

                            \DB::table('table_contents')->insertGetId([
                                        "area_division_subset_table_id" => $table_id,
                                        "content" => $content['content'],
                                        "isBlank" => $content['isBlank'],
                                        "row" => $content['row'],
                                        "column" => $content['column'],
                                        "created_at" => date("Y-m-d H:i:s"),
                                        "updated_at" => date("Y-m-d H:i:s")
                                    ]);
                          }
                        }
                      }


                     }//end of foreach sct
                    } 

                    if($scs['checkboxes'])
                    {
                      foreach ($scs['checkboxes'] as $checkbox)
                      {
                        $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $checkbox['instruction']
                            ]);

                        if($checkbox['options'])
                        {
                          foreach($checkbox['options'] as $option)
                          {
                            \DB::table('subset_checkbox_options')->insert([
                                    "checkbox_id" => $checkbox_id,
                                    "option" => $option['option']
                                ]);
                          }
                        }
                         
                      }
                    }

                    if($scs['tables'])
                    {
                     
                      foreach ($scs['tables'] as $table)
                      {
                         $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                                "subset_id" => $scs_id,
                                "instruction" => $table['instruction'],
                                "row_count" => $table['row_count'],
                                "column_count" => $table['column_count'],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                           ]);
                        
                        foreach ($table['content'] as $content)
                        {
                          if($content['content'] == null)
                              $content['isBlank'] = 1;
                          else
                             $content['isBlank'] = 0;

                          \DB::table('table_contents')->insertGetId([
                                      "area_division_subset_table_id" => $table_id,
                                      "content" => $content['content'],
                                      "row" => $content['row'],
                                      "isBlank" =>$content['isBlank'],
                                      "column" => $content['column'],
                                      "created_at" => date("Y-m-d H:i:s"),
                                      "updated_at" => date("Y-m-d H:i:s")
                                  ]);
                        }
                      }
                    }

                   }//end of foreach scs
                  } 

                  if($scf['checkboxes'])
                  {
                    foreach ($scf['checkboxes'] as $checkbox)
                    {
                      $checkbox_id =\DB::table('subset_checkboxes')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $checkbox['instruction']
                          ]);

                      if($checkbox['options'])
                      {
                        foreach($checkbox['options'] as $option)
                        {
                          \DB::table('subset_checkbox_options')->insert([
                                  "checkbox_id" => $checkbox_id,
                                  "option" => $option['option']
                              ]);
                        }
                      }
                      
                       
                    }
                  }

                  if($scf['tables'])
                  {
                   
                    foreach ($scf['tables'] as $table)
                    {
                       $table_id =  \DB::table('area_division_subset_tables')->insertGetId([
                              "subset_id" => $scf_id,
                              "instruction" => $table['instruction'],
                              "row_count" => $table['row_count'],
                              "column_count" => $table['column_count'],
                              "created_at" => date("Y-m-d H:i:s"),
                              "updated_at" => date("Y-m-d H:i:s")
                         ]);
                      foreach ($table['content'] as $content)
                      {
                        if($content['content'] == null)
                              $content['isBlank'] = 1;
                          else
                             $content['isBlank'] = 0;

                        \DB::table('table_contents')->insertGetId([
                                    "area_division_subset_table_id" => $table_id,
                                    "content" => $content['content'],
                                    "isBlank" =>$content['isBlank'] ,
                                    "row" => $content['row'],
                                    "column" => $content['column'],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                          ]);
                        }
                      }
                    }
                  }

                 }//end of foreach
                } 

                if($areadivision['tables'])
                {
                  foreach ($areadivision['tables'] as $table)
                  {
                    $table_id =  \DB::table('area_division_tables')->insertGetId([
                            "area_division_id" => $area_division_id,
                            "instruction" => $table['instruction'],
                            "row_count" => $table['row_count'],
                            "column_count" => $table['column_count'],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                       ]);
                    foreach ($table['content'] as $content)
                    {
                      if($content['content'] == null)
                          $content['isBlank'] = 1;
                      else
                         $content['isBlank'] = 0;

                      \DB::table('area_division_table_contents')->insertGetId([
                                  "area_division_table_id" => $table_id,
                                  "content" => $content['content'],
                                  "isBlank" => $content['isBlank'],
                                  "row" => $content['row'],
                                  "column" => $content['column'],
                                  "created_at" => date("Y-m-d H:i:s"),
                                  "updated_at" => date("Y-m-d H:i:s")
                              ]);
                    }
                  }
                }
             
          });
      return $data;
    }

    public function deleteSubsetCheckbox($id) : bool
    {
         $checkbox = SubsetCheckbox::find($id);
         return $checkbox->delete();
    }

    public function deleteSubsetCheckboxOption($id) : bool
    {
         $checkbox = SubsetCheckboxOptions::find($id);
         return $checkbox->delete();
    }

    public function deleteAreaDivisionTableContent(Request $request) : bool
    {
      $table = AreaDivisionTable::find($request['table_id']);
      
      if($request['type'] == 'column')
      {
        foreach($request['columns'] as $column)
        {
          $c = AreaDivisionTableContent::find($column);
          $c->delete();
         
        }
       $contents = AreaDivisionTableContent::where('area_division_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['column'] != 0)
          {
            $content['column'] = $content['column'] - 1;
            $content->update();
          }
        }
        if( $table->column_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->column_count =  $table->column_count - 1;

        $table->update();   

      }
      else if($request['type'] == 'row')
      {
        foreach($request['rows'] as $row)
        {
          $content = AreaDivisionTableContent::find($row);
          $content->delete();
        }

        $contents = AreaDivisionTableContent::where('area_division_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['row'] != 0)
          {
            $content['row'] = $content['row'] - 1;
            $content->update();
          }
        }

        if($table->row_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->row_count =  $table->row_count - 1;

        $table->update();

        foreach($contents as $content)
        {
          $content['row'] = $content['row'] - 1;
        }
      }
     
      if($table->column_count == 0 && $table->row_count == 0)
      {
         $table->delete();
      }
      return true;
        
    }

    public function deleteAreaDivisionTable($id) : bool
    {
         $table = AreaDivisionTable::find($id);
         return $table->delete();
    }

    public function deleteSubsetTableContent(Request $request) : bool
    {
      $table = SubsetTable::find($request['table_id']);
      
      if($request['type'] == 'column')
      {
        foreach($request['columns'] as $column)
        {
          $c = TableContent::find($column);
          $c->delete();
         
        }
       $contents = TableContent::where('area_division_subset_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['column'] != 0)
          {
            $content['column'] = $content['column'] - 1;
            $content->update();
          }
        }
        if( $table->column_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->column_count =  $table->column_count - 1;

        $table->update();   

      }
      else if($request['type'] == 'row')
      {
        foreach($request['rows'] as $row)
        {
          $content = TableContent::find($row);
          $content->delete();
        }

        $contents = TableContent::where('area_division_subset_table_id',$request['table_id'])->get();
        foreach($contents as $content)
        {
          if($content['row'] != 0)
          {
            $content['row'] = $content['row'] - 1;
            $content->update();
          }
        }

        if($table->row_count == 1)
        {
          $table->column_count = 0;
          $table->row_count = 0;
        }
        else
          $table->row_count =  $table->row_count - 1;

        $table->update();

        foreach($contents as $content)
        {
          $content['row'] = $content['row'] - 1;
        }
      }
     
      if($table->column_count == 0 && $table->row_count == 0)
      {
         $table->delete();
      }
      return true;
        
    }

    public function deleteSubsetTable($id) : bool
    {
         $table = SubsetTable::find($id);
         return $table->delete();
    }

}