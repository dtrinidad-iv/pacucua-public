<?php

namespace App\Models\Areas;

use Illuminate\Database\Eloquent\Model;

class AreaDivisionSubset extends Model
{
    protected $table = 'area_division_subsets';
    public $timestamps = true;
    protected $fillable = ['code','description','area_division_id','parent_subset_id','with_ratebox','with_checkbox','with_textarea'];
 
    protected $appends = ['subsets'];


    public function getSubsetsAttribute()
    {
    	return \App\Models\Areas\AreaDivisionSubset::where('parent_subset_id',$this->id)->get()->load('tables','evaluation','evidences','checkboxes');
    }

    public function tables()
    {
    	return $this->hasMany('App\Models\SubsetTables\SubsetTable','subset_id','id')->with('content');
    }

    public function evaluation()
    {
        return $this->hasOne('App\Models\Evaluations\SubsetEvaluation','subset_id','id');
    }

    public function evidences()
    {
        return $this->hasMany('App\Models\Evidences\Evidence','subset_id','id');
    }

    public function checkboxes()
    {
        return $this->hasMany('App\Models\SubsetCheckboxes\SubsetCheckbox','subset_id','id')->with('options');
    }


}