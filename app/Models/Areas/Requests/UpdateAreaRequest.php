<?php

namespace App\Models\Areas\Requests;

use App\Models\Base\BaseFormRequest;

class UpdateAreaRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
            'title' => ['required'],
            'weight_value' => ['required', 'numeric'],
            'areadivisions.*.subsets.*.code' =>  ['required'],
            'areadivisions.*.subsets.*.description' =>  ['required'],
            'areadivisions.*.subsets.*.subsets.*.code' =>  ['required'],
            'areadivisions.*.subsets.*.subsets.*.description' =>  ['required'],
            'areadivisions.*.subsets.*.subsets.*.subsets.*.code' =>  ['required'],
            'areadivisions.*.subsets.*.subsets.*.subsets.*.description' =>  ['required'],
            'areadivisions.*.code' =>  ['required'],
            'areadivisions.*.title' =>  ['required'],

      ];
    }

    public function messages()
    {
        return [
           'title.required' => 'The title field is required.',
           'areadivisions.*.title.required' => 'The area division title field is required.',
           'areadivisions.*.code.required' => 'The area division code field is required.',
           'areadivisions.*.subsets.*.code.required' => 'The subset code field is required.',
           'areadivisions.*.subsets.*.description.required' =>  'The subset description field is required.',
           'areadivisions.*.subsets.*.subsets.*.code.required' => 'The subset code field is required.',
           'areadivisions.*.subsets.*.subsets.*.description.required' =>  'The subset description field is required.',
           'areadivisions.*.subsets.*.subsets.*.subsets.*.code.required' => 'The subset code field is required.',
           'areadivisions.*.subsets.*.subsets.*.subsets.*.description.required' =>  'The subset description field is required.',
        ];
    }
}
