<?php

namespace App\Models\Areas;

use Illuminate\Database\Eloquent\Model;

class AreaDivision extends Model
{
    protected $table = 'area_divisions';
    public $timestamps = true;
    protected $fillable = ['code','title','area_id','criteria'];

    public function subsets()
    {
         return $this->hasMany('App\Models\Areas\AreaDivisionSubset')->where('parent_subset_id', NULL);
    }

    public function area()
    {
    	return $this->belongsTo('App\Models\Areas\Area');
    }
    
    public function evaluation()
    {
        return $this->hasOne('App\Models\Evaluations\AreaDivisionEvaluation','area_division_id','id');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\AreaDivisionTables\AreaDivisionTable','area_division_id','id')->with('content');
    }
}
