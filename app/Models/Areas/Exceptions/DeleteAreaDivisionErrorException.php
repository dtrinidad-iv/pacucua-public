<?php

namespace App\Models\Areas\Exceptions;

class DeleteAreaDivisionErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_area_division_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
