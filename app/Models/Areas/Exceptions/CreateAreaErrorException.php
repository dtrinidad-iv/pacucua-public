<?php

namespace App\Models\Areas\Exceptions;

class CreateAreaErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_area_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
