<?php

namespace App\Models\Areas\Exceptions;

class DeleteAreaErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_area_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
