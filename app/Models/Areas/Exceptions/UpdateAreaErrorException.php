<?php

namespace App\Models\Areas\Exceptions;

class UpdateAreaErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_area_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
