<?php

namespace App\Models\Areas\Exceptions;

class AreaNotFoundErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'area_not_found',
              'message' => $this->getMessage()
          ],404);
    }
}
