<?php

namespace App\Models\Areas;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    public $timestamps = true;
    protected $fillable = ['code','title','weight_value','program_id','introduction'];

    public function areadivisions()
    {
        return $this->hasMany('App\Models\Areas\AreaDivision');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Programs\Program');
    }

}
