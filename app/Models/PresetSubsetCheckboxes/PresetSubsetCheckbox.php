<?php

namespace App\Models\PresetSubsetCheckboxes;

use Illuminate\Database\Eloquent\Model;

class PresetSubsetCheckbox extends Model
{
    protected $table = 'preset_subset_checkboxes';
    public $timestamps = false;
    protected $fillable = ['subset_id','instruction'];

  	public function options()
    {
    	return $this->hasMany('App\Models\PresetSubsetCheckboxes\PresetSubsetCheckboxOptions','checkbox_id','id');
    }

}
