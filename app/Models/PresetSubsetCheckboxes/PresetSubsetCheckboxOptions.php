<?php

namespace App\Models\PresetSubsetCheckboxes;

use Illuminate\Database\Eloquent\Model;

class PresetSubsetCheckboxOptions extends Model
{
    protected $table = 'preset_subset_checkbox_options';
    public $timestamps = false;
    protected $fillable = ['checkbox_id','option'];

  
}
