<?php

namespace App\Models\Evaluations\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Evaluations\SubsetEvaluation;
use App\Models\SubsetTables\TableContent;
use App\Models\Evaluations\AreaDivisionEvaluation;
use App\Models\SubsetCheckboxes\SubsetCheckboxOptions;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class SubsetEvaluationRepository extends BaseRepository implements SubsetEvaluationRepositoryInterface
{
    /**
     * SubsetEvaluationRepository constructor.
     *
     * @param SubsetEvaluation $subset_evaluation
     */
    public function __construct(SubsetEvaluation $subset_evaluation)
    {
        parent::__construct($subset_evaluation);
        $this->model = $subset_evaluation;
    }

    /**
     * @param array $data
     *
     * @return SubsetEvaluation
     * @throws CreateSubsetEvaluationErrorException
     */
    public function createSubsetEvaluation(array $data) : array
    {
        try {

            return $data;
         //  return $this->create($data);

       

        return $data;

        } catch (QueryException $e) {
            throw new CreateSubsetEvaluationErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return SubsetEvaluation
     * @throws SubsetEvaluationNotFoundErrorException
     */
    public function findSubsetEvaluationById(int $id) : SubsetEvaluation
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new SubsetEvaluationNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateSubsetEvaluationErrorException
     */
    public function updateSubsetEvaluation(array $data):bool
    {
        try {
            return $data;
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateSubsetEvaluationErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteSubsetEvaluation() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteSubsetEvaluationErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listSubsetEvaluations($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }


    public function saveSubsetEvaluation(array $data)
    {
        
        foreach($data['areadivisions'] as $areaDiv)
        {
            if($areaDiv['evaluation'] == null)
            {
                \DB::transaction(function() use ($areaDiv)
                {
                    \DB::table('area_division_evaluations')->insertGetId([
                           "area_division_id" => $areaDiv['id'],
                           "comment" => $areaDiv['comment']
                         ]);
                });
            }
            else
            {
                $comment = AreaDivisionEvaluation::where('id', $areaDiv['id']);
                if($comment)
                    $comment->update($areaDiv['evaluation']);
            }
            
            if($areaDiv['subsets'])
            {
                foreach($areaDiv['subsets'] as $s1)
                {
                    $subset1 = SubsetEvaluation::where('subset_id', $s1['id']);
                    if($subset1)
                        $subset1->update($s1['evaluation']);

                    if($s1['tables'])
                    {
                        foreach($s1['tables'] as $table)
                        {
                            foreach($table['content'] as $c_row)
                            {
                                $content = TableContent::find($c_row['id']);
                                if($content)
                                {
                                    $content->update($c_row);
                                }
                            }
                        }
                    }

                    if($s1['checkboxes'])
                    {
                        foreach($s1['checkboxes'] as $checkbox)
                        {
                            foreach($checkbox['options'] as $option)
                            {
                                $opt = SubsetCheckboxOptions::where('id',$option['id']);
                                if($opt)
                                    $opt->update($option);
                            }
                        }
                    }

                    if($s1['subsets'])
                    {
                        foreach($s1['subsets'] as $s2)
                        {
                            $subset2 = SubsetEvaluation::where('subset_id', $s2['id']);
                            if($subset2)
                                $subset2->update($s2['evaluation']);

                            if($s2['subsets'])
                            {
                                foreach($s2['subsets'] as $s3)
                                {
                                    $subset3 = SubsetEvaluation::where('subset_id', $s3['id']);
                                    if($subset3)
                                        $subset3->update($s3['evaluation']);
                                    
                                    if($s3['checkboxes'])
                                    {
                                        foreach($s3['checkboxes'] as $checkbox)
                                        {
                                            foreach($checkbox['options'] as $option)
                                            {
                                                $opt = SubsetCheckboxOptions::where('id',$option['id']);
                                                if($opt)
                                                    $opt->update($option);
                                            }
                                        }
                                    }

                                    if($s3['tables'])
                                    {
                                        foreach($s3['tables'] as $table)
                                        {
                                            foreach($table['content'] as $c_row)
                                            {
                                                $content = TableContent::find($c_row['id']);
                                                if($content)
                                                {
                                                    $content->update($c_row);
                                                }
                                            }
                                        }
                                    }

                                    
                                }
                            }

                            if($s2['checkboxes'])
                            {
                                foreach($s2['checkboxes'] as $checkbox)
                                {
                                    foreach($checkbox['options'] as $option)
                                    {
                                        $opt = SubsetCheckboxOptions::where('id',$option['id']);
                                        if($opt)
                                            $opt->update($option);
                                    }
                                }
                            }

                            if($s2['tables'])
                            {
                                foreach($s2['tables'] as $table)
                                {
                                    foreach($table['content'] as $c_row)
                                    {
                                        $content = TableContent::where('id',$c_row['id']);
                                        if($content)
                                        {
                                            $content->update($c_row);
                                        }
                                    }
                                }
                            }
                        }
                    }



                }
            }
            

            
          
        }
        return $data;
    }

  


}
