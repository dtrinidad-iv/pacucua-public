<?php

namespace App\Models\Evaluations\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Evaluations\Evaluation;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface EvaluationRepositoryInterface extends BaseRepositoryInterface
{
    public function createEvaluation(array $data): array;

    public function findEvaluationById(int $id) : Evaluation;

    public function updateEvaluation(array $data) : bool;

    public function deleteEvaluation() : bool;

    public function listEvaluations($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    // function getAllEvaluations(Request $request);
}
