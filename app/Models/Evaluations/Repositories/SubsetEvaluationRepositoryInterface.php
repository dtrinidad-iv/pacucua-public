<?php

namespace App\Models\Evaluations\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Evaluations\SubsetEvaluation;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface SubsetEvaluationRepositoryInterface extends BaseRepositoryInterface
{
    public function createSubsetEvaluation(array $data): array;

    public function findSubsetEvaluationById(int $id) : SubsetEvaluation;

    public function updateSubsetEvaluation(array $data) : bool;

    public function deleteSubsetEvaluation() : bool;

    public function listSubsetEvaluations($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    // function getAllSubsetEvaluations(Request $request);
}
