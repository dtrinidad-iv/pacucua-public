<?php

namespace App\Models\Evaluations\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Evaluations\Evaluation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class EvaluationRepository extends BaseRepository implements EvaluationRepositoryInterface
{
    /**
     * EvaluationRepository constructor.
     *
     * @param Evaluation $evaluation
     */
    public function __construct(Evaluation $evaluation)
    {
        parent::__construct($evaluation);
        $this->model = $evaluation;
    }

    /**
     * @param array $data
     *
     * @return Evaluation
     * @throws CreateEvaluationErrorException
     */
    public function createEvaluation(array $data) : array
    {
        try {

            return $data;
         //  return $this->create($data);

       

        return $data;

        } catch (QueryException $e) {
            throw new CreateEvaluationErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Evaluation
     * @throws EvaluationNotFoundErrorException
     */
    public function findEvaluationById(int $id) : Evaluation
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new EvaluationNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateEvaluationErrorException
     */
    public function updateEvaluation(array $data):bool
    {
        try {
            return $data;
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateEvaluationErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteEvaluation() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteEvaluationErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listEvaluations($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }


    public function saveEvaluation(array $data)
    {
        // try {
        return $data;
                foreach($data['subsets'] as $s1)
                {
                    $subset = Evaluation::find($s1['id']);
                    if($subset)
                    {
                       // return $s1['evaluation'];
                        $subset->update($s1['evaluation']);
                    }
                  
                }
                return $data;
        // } catch (QueryException $e) {
        //     throw new UpdateEvaluationErrorException($e);
        // }
    }

  


}
