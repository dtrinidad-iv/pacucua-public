<?php

namespace App\Models\Evaluations;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $table = 'evaluations';
    public $timestamps = true;
    protected $fillable = ['subset_id', 'rate','checkbox','textarea'];

    public function subset()
    {
    	return $this->belongsTo('App\Models\Areas\AreaDivisionSubset'); 
    }

}
