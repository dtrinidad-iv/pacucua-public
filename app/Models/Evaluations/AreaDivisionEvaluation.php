<?php

namespace App\Models\Evaluations;

use Illuminate\Database\Eloquent\Model;

class AreaDivisionEvaluation extends Model
{
    protected $table = 'area_division_evaluations';
    public $timestamps = false;
    protected $fillable = ['area_division_id', 'comment'];

    public function areadivision()
    {
    	return $this->belongsTo('App\Models\Areas\AreaDivision'); 
    }

}
