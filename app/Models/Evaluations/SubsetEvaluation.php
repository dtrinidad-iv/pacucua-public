<?php

namespace App\Models\Evaluations;

use Illuminate\Database\Eloquent\Model;

class SubsetEvaluation extends Model
{
    protected $table = 'subset_evaluations';
    public $timestamps = true;
    protected $fillable = ['subset_id', 'rate','checkbox','textarea'];

    public function subset()
    {
    	return $this->belongsTo('App\Models\Areas\AreaDivisionSubset'); 
    }

}
