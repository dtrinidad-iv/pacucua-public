<?php

namespace App\Models\SubsetCheckboxes;

use Illuminate\Database\Eloquent\Model;

class SubsetCheckboxOptions extends Model
{
    protected $table = 'subset_checkbox_options';
    public $timestamps = false;
    protected $fillable = ['checkbox_id','option'];

  
}
