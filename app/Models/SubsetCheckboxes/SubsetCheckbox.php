<?php

namespace App\Models\SubsetCheckboxes;

use Illuminate\Database\Eloquent\Model;

class SubsetCheckbox extends Model
{
    protected $table = 'subset_checkboxes';
    public $timestamps = false;
    protected $fillable = ['subset_id','instruction'];

  	public function options()
    {
    	return $this->hasMany('App\Models\SubsetCheckboxes\SubsetCheckboxOptions','checkbox_id','id');
    }

}
