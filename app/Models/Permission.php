<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        return [

        	'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_departments',
            'add_departments',
            'edit_departments',
            'delete_departments',

            'view_programs',
            'add_programs',
            'edit_programs',
            'delete_programs',

            'view_areas',
            'add_areas',
            'edit_areas',
            'delete_areas',



        ];
    }
}
