<?php

namespace App\Models\PresetSubsetTables;

use Illuminate\Database\Eloquent\Model;

class PresetSubsetTable extends Model
{
    protected $table = 'preset_area_division_subset_tables';
    public $timestamps = true;
    protected $fillable = ['subset_id','row_count','column_count','instruction'];

  	public function content()
    {
    	return $this->hasMany('App\Models\PresetSubsetTables\PresetTableContent','subset_table_id','id');
    }

}
