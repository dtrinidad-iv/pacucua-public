<?php

namespace App\Models\PresetSubsetTables;

use Illuminate\Database\Eloquent\Model;

class PresetTableContent extends Model
{
    protected $table = 'preset_table_contents';
    public $timestamps = false;
    protected $fillable = ['subset_table_id','column','content','row'];

  
}
