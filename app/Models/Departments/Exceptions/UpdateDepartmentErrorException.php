<?php

namespace App\Models\Departments\Exceptions;

class UpdateDepartmentErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_department_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
