<?php

namespace App\Models\Departments\Exceptions;

class CreateDepartmentErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_department_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
