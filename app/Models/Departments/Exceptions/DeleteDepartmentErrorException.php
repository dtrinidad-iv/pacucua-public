<?php

namespace App\Models\Departments\Exceptions;

class DeleteDepartmentErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_department_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
