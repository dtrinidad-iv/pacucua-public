<?php

namespace App\Models\Departments\Exceptions;

class DepartmentNotFoundErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'department_not_found',
              'message' => $this->getMessage()
          ],404);
    }
}
