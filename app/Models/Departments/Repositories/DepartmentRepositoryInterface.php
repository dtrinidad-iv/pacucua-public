<?php

namespace App\Models\Departments\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Departments\Department;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface DepartmentRepositoryInterface extends BaseRepositoryInterface
{
    public function createDepartment(array $data): array;

    public function findDepartmentById(int $id) : Department;

    public function updateDepartment(array $data) : bool;

    public function deleteDepartment() : bool;

    public function listDepartments($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllDepartments(Request $request);
}
