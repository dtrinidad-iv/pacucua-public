<?php

namespace App\Models\Departments\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Departments\Department;
use App\Models\Departments\Exceptions\DepartmentNotFoundErrorException;
use App\Models\Departments\Exceptions\CreateDepartmentErrorException;
use App\Models\Departments\Exceptions\UpdateDepartmentErrorException;
use App\Models\Departments\Exceptions\DeleteDepartmentErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    /**
     * DepartmentRepository constructor.
     *
     * @param Department $department
     */
    public function __construct(Department $department)
    {
        parent::__construct($department);
        $this->model = $department;
    }

    /**
     * @param array $data
     *
     * @return Department
     * @throws CreateDepartmentErrorException
     */
    public function createDepartment(array $data) : array
    {
        try {

          // return $this->create($data);

         \DB::transaction(function() use ($data)
          {
            $department_id =  \DB::table('departments')->insertGetId([
                   "name" => $data['name'],
                   "created_at" => date("Y-m-d H:i:s"),
                   "updated_at" => date("Y-m-d H:i:s")
                 ]);

          });

        return $data;

        } catch (QueryException $e) {
            throw new CreateDepartmentErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Department
     * @throws DepartmentNotFoundErrorException
     */
    public function findDepartmentById(int $id) : Department
    {
        try {
            return $this->findOneOrFail($id)->load('programs');
        } catch (ModelNotFoundException $e) {
            throw new DepartmentNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateDepartmentErrorException
     */
    public function updateDepartment(array $data) : bool
    {
        try {
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateDepartmentErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteDepartment() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeleteDepartmentErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listDepartments($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllDepartments(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $departments = $this->model->where('name', 'like', '%' . $r["filter"] . '%')->with('programs','programs.areas')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $departments = $this->model->with('programs','programs.areas')->paginate(5);
               return response()->json(compact('departments'));
          }else{
            $departments = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $departments;
    }


}
