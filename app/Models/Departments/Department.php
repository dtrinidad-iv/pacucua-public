<?php

namespace App\Models\Departments;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    public $timestamps = true;
    protected $fillable = ['name'];


    public function programs(){
        return $this->hasMany('App\Models\Programs\Program');
    }
}
