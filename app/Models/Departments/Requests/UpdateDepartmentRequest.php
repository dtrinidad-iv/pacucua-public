<?php

namespace App\Models\Departments\Requests;

use App\Models\Base\BaseFormRequest;

class UpdateDepartmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'name' => ['required', 'unique:departments,name,' . $this->id],
      ];
    }
}
