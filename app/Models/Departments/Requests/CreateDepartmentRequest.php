<?php

namespace App\Models\Departments\Requests;

use App\Models\Base\BaseFormRequest;

class CreateDepartmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:departments']

        ];
    }

    public function messages()
    {
        return [
           'name.required' => 'The name field is required.'
        ];
    }
}
