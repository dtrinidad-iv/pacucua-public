<?php

namespace App\Models\Evidences\Exceptions;

class UpdateEvidenceErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'update_evidence_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
