<?php

namespace App\Models\Evidences\Exceptions;

class EvidenceNotFoundErrorException extends \Exception
{
    public function render($request)
    {
         return response()->json([
              'error' => 'evidence_not_found',
              'message' => $this->getMessage()
          ],404);
    }
}
