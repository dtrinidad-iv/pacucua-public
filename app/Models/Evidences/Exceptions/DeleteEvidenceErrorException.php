<?php

namespace App\Models\Evidences\Exceptions;

class DeleteEvidenceErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_evidence_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
