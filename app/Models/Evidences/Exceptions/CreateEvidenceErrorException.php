<?php

namespace App\Models\Evidences\Exceptions;

class CreateEvidenceErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_evidence_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
