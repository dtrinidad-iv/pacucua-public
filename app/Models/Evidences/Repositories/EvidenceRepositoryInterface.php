<?php

namespace App\Models\Evidences\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\Evidences\Evidence;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface EvidenceRepositoryInterface extends BaseRepositoryInterface
{
    public function createEvidence(Request $request);

    public function findEvidenceById(int $id) : Evidence;

    public function updateEvidence(array $data) : bool;

    public function deleteEvidence() : bool;

    public function listEvidences($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllEvidences(Request $request);
}
