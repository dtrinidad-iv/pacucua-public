<?php

namespace App\Models\Evidences\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\Evidences\Evidence;
use App\Models\Evidences\Exceptions\EvidenceNotFoundErrorException;
use App\Models\Evidences\Exceptions\CreateEvidenceErrorException;
use App\Models\Evidences\Exceptions\UpdateEvidenceErrorException;
use App\Models\Evidences\Exceptions\DeleteEvidenceErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class EvidenceRepository extends BaseRepository implements EvidenceRepositoryInterface
{
    /**
     * EvidenceRepository constructor.
     *
     * @param Evidence $evidence
     */
    public function __construct(Evidence $evidence)
    {
        parent::__construct($evidence);
        $this->model = $evidence;
    }

    /**
     * @param array $data
     *
     * @return Evidence
     * @throws CreateEvidenceErrorException
     */
    public function createEvidence(Request $request)
    {
        try {

               if($file = $request->file('file'))
               {
                  $path = $file->move('uploads/evidences',$file->getClientOriginalName());
              
                    $id = \DB::table('evidences')->insertGetId([
                        "subset_id" => $request['subset_id'],
                        "evidence" => '/uploads/evidences/'.$file->getClientOriginalName(),
                        "filename" => $file->getClientOriginalName(),
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    ]);

                    return $id;
                 
               }

        } catch (QueryException $e) {
            throw new CreateEvidenceErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Evidence
     * @throws EvidenceNotFoundErrorException
     */
    public function findEvidenceById(int $id) : Evidence
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new EvidenceNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateEvidenceErrorException
     */
    public function updateEvidence(array $data) : bool
    {
        try {




            return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateEvidenceErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteEvidence() : bool
    {

        try{
          $tempImage = $this->model->evidence;
          $this->delete();
          return \File::delete(public_path().$tempImage);
          
        } catch (QueryException $e) {
            throw new DeleteEvidenceErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listEvidences($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllEvidences(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $Evidences = $this->model->where('name', 'like', '%' . $r["filter"] . '%')->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $Evidences = $this->model->paginate(5);
               return response()->json(compact('Evidences'));
          }else{
            $Evidences = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $Evidences;
    }


}
