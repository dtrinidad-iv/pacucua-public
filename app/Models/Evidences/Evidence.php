<?php

namespace App\Models\Evidences;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    protected $table = 'evidences';
    public $timestamps = true;
    protected $fillable = ['subset_id','evidence','filename'];

}
