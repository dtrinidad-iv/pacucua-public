<?php

namespace App\Models\Evidences\Requests;

use App\Models\Base\BaseFormRequest;

class CreateEvidenceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }
}
