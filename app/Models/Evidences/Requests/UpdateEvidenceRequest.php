<?php

namespace App\Models\Evidences\Requests;

use App\Models\Base\BaseFormRequest;

class UpdateEvidenceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          // 'name' => ['required', 'unique:evidences,name,' . $this->id]
      ];
    }
}
