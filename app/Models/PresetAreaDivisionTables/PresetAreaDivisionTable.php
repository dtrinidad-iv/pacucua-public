<?php

namespace App\Models\PresetAreaDivisionTables;

use Illuminate\Database\Eloquent\Model;

class PresetAreaDivisionTable extends Model
{
    protected $table = 'preset_area_division_tables';
    public $timestamps = true;
    protected $fillable = ['area_division_id','row_count','column_count'];

  	public function content()
    {
    	return $this->hasMany('App\Models\PresetAreaDivisionTables\PresetAreaDivisionTableContent','table_id','id');
    }

}
