<?php

namespace App\Models\PresetAreaDivisionTables;

use Illuminate\Database\Eloquent\Model;

class PresetAreaDivisionTableContent extends Model
{
    protected $table = 'preset_area_division_table_contents';
    public $timestamps = false;
    protected $fillable = ['table_id','column','content','row'];

  
}
