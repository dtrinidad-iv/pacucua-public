<?php

namespace App\Models\PresetAreaDivisionTables\Repositories;

use App\Models\Base\BaseRepository;
use App\Models\PresetAreaDivisionTables\PresetAreaDivisionTable;
use App\Models\PresetAreaDivisionTables\Exceptions\PresetAreaDivisionTableNotFoundErrorException;
use App\Models\PresetAreaDivisionTables\Exceptions\CreatePresetAreaDivisionTableErrorException;
use App\Models\PresetAreaDivisionTables\Exceptions\UpdatePresetAreaDivisionTableErrorException;
use App\Models\PresetAreaDivisionTables\Exceptions\DeletePresetAreaDivisionTableErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class PresetAreaDivisionTableRepository extends BaseRepository implements PresetAreaDivisionTableRepositoryInterface
{
    /**
     * PresetAreaDivisionTableRepository constructor.
     *
     * @param PresetAreaDivisionTable $presetAreadivisiontable
     */
    public function __construct(PresetAreaDivisionTable $presetAreadivisiontable)
    {
        parent::__construct($presetAreadivisiontable);
        $this->model = $presetAreadivisiontable;
    }

    /**
     * @param array $data
     *
     * @return PresetAreaDivisionTable
     * @throws CreatePresetAreaDivisionTableErrorException
     */
    public function createPresetAreaDivisionTable(array $data) : array
    {
        try {

           return $this->create($data);


        return $data;

        } catch (QueryException $e) {
            throw new CreatePresetAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return PresetAreaDivisionTable
     * @throws PresetAreaDivisionTableNotFoundErrorException
     */
    public function findPresetAreaDivisionTableById(int $id) : PresetAreaDivisionTable
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new PresetAreaDivisionTableNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdatePresetAreaDivisionTableErrorException
     */
    public function updatePresetAreaDivisionTable(array $data) : bool
    {
        try {
          return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdatePresetAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deletePresetAreaDivisionTable() : bool
    {

        try{

          return $this->delete();

        } catch (QueryException $e) {
            throw new DeletePresetAreaDivisionTableErrorException($e);
        }
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listPresetAreaDivisionTables($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function getAllPresetAreaDivisionTables(Request $request)
    {

       $r = $request;

       if (isset($r["sort"])){
            $sort = explode("|",$r["sort"]);
          }

          if (isset($r["filter"])) {
            $presetAreadivisiontables = $this->model->where('name', 'like', '%' . $r["filter"] . '%')
                  ->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }else if(!isset($r["sort"])){
               $presetAreadivisiontables = $this->model->paginate(5);
               return response()->json(compact('presetAreadivisiontables'));
          }else{
            $presetAreadivisiontables = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
          }

      return $presetAreadivisiontables;
    }
}
