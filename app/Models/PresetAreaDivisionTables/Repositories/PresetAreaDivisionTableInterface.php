<?php

namespace App\Models\PresetDivisionTables\Repositories;

use App\Models\Base\BaseRepositoryInterface;
use App\Models\PresetDivisionTables\PresetDivisionTable;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface PresetDivisionTableRepositoryInterface extends BaseRepositoryInterface
{
    public function createPresetDivisionTable(array $data);

    public function findPresetDivisionTableById(int $id) : PresetDivisionTable;

    public function updatePresetDivisionTable(array $data) : bool;

    public function deletePresetDivisionTable() : bool;

    public function listPresetDivisionTables($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    function getAllPresetDivisionTables(Request $request);
}
