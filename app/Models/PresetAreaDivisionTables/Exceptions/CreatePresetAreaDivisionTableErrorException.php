<?php

namespace App\Models\PresetAreaDivisionTables\Exceptions;

class CreatePresetAreaDivisionTableErrorException extends \Exception
{

    public function render($request)
    {
         return response()->json([
              'error' => 'create_presetarea_division_table_query_exception',
              'message' => $this->getMessage()
          ],500);
    }

}
