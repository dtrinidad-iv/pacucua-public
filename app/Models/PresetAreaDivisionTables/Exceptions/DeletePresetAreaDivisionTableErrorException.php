<?php

namespace App\Models\PresetAreaDivisionTables\Exceptions;

class DeletePresetAreaDivisionTableErrorException extends \Exception
{

  public function render($request)
  {
       return response()->json([
            'error' => 'delete_preset_area_division_table_query_exception',
            'message' => $this->getMessage()
        ],500);
  }

}
