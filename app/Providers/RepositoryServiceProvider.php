<?php

namespace App\Providers;

use App\Models\Categories\Repositories\CategoryRepository;
use App\Models\Categories\Repositories\CategoryRepositoryInterface;

use App\Models\ProductCategories\Repositories\ProductCategoryRepository;
use App\Models\ProductCategories\Repositories\ProductCategoryRepositoryInterface;

use App\Models\Areas\Repositories\AreaRepository;
use App\Models\Areas\Repositories\AreaRepositoryInterface;

use App\Models\Departments\Repositories\DepartmentRepository;
use App\Models\Departments\Repositories\DepartmentRepositoryInterface;

use App\Models\Programs\Repositories\ProgramRepository;
use App\Models\Programs\Repositories\ProgramRepositoryInterface;

use App\Models\PresetAreas\Repositories\PresetAreaRepository;
use App\Models\PresetAreas\Repositories\PresetAreaRepositoryInterface;

use App\Models\Evidences\Repositories\EvidenceRepository;
use App\Models\Evidences\Repositories\EvidenceRepositoryInterface;

use App\Models\Evaluations\Repositories\SubsetEvaluationRepository;
use App\Models\Evaluations\Repositories\SubsetEvaluationRepositoryInterface;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );

        $this->app->bind(
            ProductCategoryRepositoryInterface::class,
            ProductCategoryRepository::class
        );

        $this->app->bind(
            AreaRepositoryInterface::class,
            AreaRepository::class
        );

        $this->app->bind(
            DepartmentRepositoryInterface::class,
            DepartmentRepository::class
        );

        $this->app->bind(
            ProgramRepositoryInterface::class,
            ProgramRepository::class
        );

        $this->app->bind(
            PresetAreaRepositoryInterface::class,
            PresetAreaRepository::class
        );

        $this->app->bind(
            EvidenceRepositoryInterface::class,
            EvidenceRepository::class
        );

        $this->app->bind(
            SubsetEvaluationRepositoryInterface::class,
            SubsetEvaluationRepository::class
        );

    }
}
