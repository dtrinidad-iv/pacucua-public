<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\User;
use App\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
            // disable fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");

            // enable back fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $this->command->info('Default Permissions added.');

        // Confirm roles needed
        if ($this->command->confirm('Create Roles for user (SuperAdmin,Admin,Evaluator,Accreditor)?', true)) {

            // Ask for roles from input
            // $input_roles = $this->command->ask('Enter roles in comma separate format.', 'SuperAdmin,Admin,Evaluator,Accreditor');

            $input_roles = 'SuperAdmin,Admin,Evaluator,Accreditor';
            
            // Explode roles
            $roles_array = explode(',', $input_roles);

            // add roles
            foreach($roles_array as $role) {
                $role = Role::firstOrCreate(['name' => trim($role)]);

                if( $role->name == 'SuperAdmin' ) {
                    // assign all permissions
                    $role->syncPermissions(Permission::all());
                    $this->command->info('SuperAdmin granted all the permissions');
                }
                else {
                    // for others by default only read access
                    $role->syncPermissions(Permission::where('name', 'LIKE', 'view_%')->get());
                }

                // create one user for each role
                if($role->name == 'SuperAdmin')
                    $this->createUser($role);
            }

            $this->command->info('Roles ' . $input_roles . ' added successfully');

        } else {
            Role::firstOrCreate(['name' => 'User']);
            $this->command->info('Added only default user role.');

            // for others by default only read access
            $role->syncPermissions(Permission::where('name', 'LIKE', 'view_%')->get());
        }

        $this->command->info('Data seeded');
        $this->command->warn('All done :)');
    }

    /**
     * Create a user with given role
     *
     * @param $role
     */
    private function createUser($role)
    {
        $user = factory(User::class)->create();
        $user->name =  'SuperAdmin';
        $user->email =  'superadmin@email.com';
        $user->update();
        $user->assignRole($role->name);

        if( $role->name == 'SuperAdmin' ) {
            $this->command->info('Here is your superadmin details to login:');
            $this->command->warn($user->email);
            $this->command->warn('Password is "secret"');
        }
    }
}
