<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDivisionSubsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_division_subsets', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('area_division_id')->unsigned();
          $table->integer('parent_subset_id')->nullable();
          $table->string('code');
          $table->mediumtext('description');
          $table->boolean('with_ratebox')->default(0);
          $table->boolean('with_checkbox')->default(0);
          $table->boolean('with_textarea')->default(0);
          $table->timestamps();

          $table->foreign('area_division_id')
                ->references('id')->on('area_divisions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_division_subsets');
    }
}
