<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresetAreaDivisionTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preset_area_division_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_division_id')->unsigned();
            $table->integer('row_count')->default(0);
            $table->integer('column_count')->default(0);
            $table->string('instruction',500)->nullable();
            $table->timestamps();
        });

        Schema::table('preset_area_division_tables', function (Blueprint $table) {
            $table->foreign('area_division_id')
                  ->references('id')->on('preset_area_divisions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preset_area_division_tables');
    }
}
