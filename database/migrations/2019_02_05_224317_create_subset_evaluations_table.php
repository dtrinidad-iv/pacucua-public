<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsetEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subset_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subset_id')->unsigned();
            $table->decimal('rate')->nullable();
            $table->boolean('checkbox')->nullable();
            $table->mediumtext('textarea')->nullable();
            $table->timestamps();

            $table->foreign('subset_id')
                  ->references('id')->on('area_division_subsets')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subset_evaluations');
    }
}
