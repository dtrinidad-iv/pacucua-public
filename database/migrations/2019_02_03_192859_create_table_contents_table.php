<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_division_subset_table_id')->unsigned();
            $table->integer('row');
            $table->integer('column');
            $table->mediumtext('content')->nullable();
            $table->boolean('isBlank')->default(0);
            $table->foreign('area_division_subset_table_id')
                  ->references('id')->on('area_division_subset_tables')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_contents');
    }
}
