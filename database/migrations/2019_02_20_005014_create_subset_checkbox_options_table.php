<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsetCheckboxOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subset_checkbox_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checkbox_id')->unsigned();
            $table->mediumtext('option');
            $table->foreign('checkbox_id')
                  ->references('id')->on('subset_checkboxes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subset_checkbox_options');
    }
}
