<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresetSubsetCheckboxOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preset_subset_checkbox_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checkbox_id')->unsigned();
            $table->mediumtext('option');
            $table->foreign('checkbox_id')
                  ->references('id')->on('preset_subset_checkboxes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preset_subset_checkbox_options');
    }
}
