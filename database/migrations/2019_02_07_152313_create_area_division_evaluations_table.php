<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDivisionEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_division_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_division_id')->unsigned();
            $table->mediumtext('comment')->nullable();

            $table->foreign('area_division_id')
                  ->references('id')->on('area_divisions')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_division_evaluations');
    }
}
