<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDivisionTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_division_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_division_id')->unsigned();
            $table->integer('row_count')->default(0);
            $table->integer('column_count')->default(0);
            $table->string('instruction',500)->nullable();
            $table->timestamps();
        });

        Schema::table('area_division_tables', function (Blueprint $table) {
            $table->foreign('area_division_id')
                  ->references('id')->on('area_divisions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_division_tables');
    }
}
