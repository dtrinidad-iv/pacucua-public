<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDivisionSubsetTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_division_subset_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subset_id')->unsigned();
            $table->integer('row_count')->default(0);
            $table->integer('column_count')->default(0);
            $table->string('instruction',500)->nullable();
            $table->foreign('subset_id')
                  ->references('id')->on('area_division_subsets')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_division_subset_tables');
    }
}
