<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsetCheckboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subset_checkboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subset_id')->unsigned();
            $table->string('instruction',500)->nullable();
            $table->foreign('subset_id')
                  ->references('id')->on('area_division_subsets')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subset_checkboxes');
    }
}
