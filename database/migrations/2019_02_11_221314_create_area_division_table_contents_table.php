<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDivisionTableContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_division_table_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_division_table_id')->unsigned();
            $table->integer('row');
            $table->integer('column');
            $table->mediumtext('content')->nullable();
            $table->boolean('isBlank')->default(0);
            
            $table->timestamps();
        });

         Schema::table('area_division_table_contents', function (Blueprint $table) {
            $table->foreign('area_division_table_id')
                  ->references('id')->on('area_division_tables')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_division_table_contents');
    }
}
