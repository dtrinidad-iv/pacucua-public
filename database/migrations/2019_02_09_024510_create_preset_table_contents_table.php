<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresetTableContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preset_table_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subset_table_id')->unsigned();
            $table->integer('row');
            $table->integer('column');
            $table->mediumtext('content')->nullable();
            $table->boolean('isBlank')->default(0);
            $table->foreign('subset_table_id')
                  ->references('id')->on('preset_area_division_subset_tables')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preset_table_contents');
    }
}
