<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {



});





// AUTH
Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');

Route::group(['middleware' => 'jwt.auth'], function(){
  Route::resource('users', 'UserController');
  Route::resource('roles', 'RoleController');
  Route::get('permissions', 'RoleController@getPermissions');
  Route::get('role_permissions', 'RoleController@checkRolePermission');
  Route::get('auth/user', 'AuthController@user');
  Route::post('auth/logout', 'AuthController@logout');

  // DEPARTMENTS
  Route::resource('departments', 'Api\DepartmentController');

  // PROGRAMS
  Route::resource('programs', 'Api\ProgramController');
  Route::get('fetch-department-programs/{id}', 'Api\ProgramController@getProgramsByDepartment');

  // AREAS
  Route::resource('areas', 'Api\AreaController');
  Route::get('fetch-program-areas/{id}', 'Api\AreaController@getAreasByProgram');

  // AREA DIVISIONS
  Route::delete('delete-area-division/{id}', 'Api\AreaController@deleteAreaDivision');
  Route::delete('delete-division-subset/{id}', 'Api\AreaController@deleteDivisionSubset');
  Route::get('fetch-area-division/{id}', 'Api\AreaController@getAreaDivision');
  Route::resource('area-division-table','Api\AreaDivisionTableController');
  Route::delete('delete-area-division-table/{id}', 'Api\AreaController@deleteAreaDivisionTable');
  Route::post('delete-area-division-table-content', 'Api\AreaController@deleteAreaDivisionTableContent');
  Route::delete('delete-subset-checkbox/{id}', 'Api\AreaController@deleteSubsetCheckbox');
  Route::delete('delete-subset-checkbox-option/{id}', 'Api\AreaController@deleteSubsetCheckboxOption');
  Route::delete('delete-subset-table/{id}', 'Api\AreaController@deleteSubsetTable');
  Route::post('delete-subset-table-content', 'Api\AreaController@deleteSubsetTableContent');

  // PRESET AREAS
  Route::resource('preset-areas', 'Api\PresetAreaController');
  Route::post('add-preset-area', 'Api\AreaController@addPresetArea');

  Route::delete('delete-preset-area-division/{id}', 'Api\PresetAreaController@deletePresetAreaDivision');
  Route::delete('delete-preset-division-subset/{id}', 'Api\PresetAreaController@deletePresetDivisionSubset');
  Route::resource('preset-area-division-table','Api\PresetAreaController');
  Route::delete('delete-preset-area-division-table/{id}', 'Api\PresetAreaController@deletePresetAreaDivisionTable');
  Route::post('delete-preset-area-division-table-content', 'Api\PresetAreaController@deletePresetAreaDivisionTableContent');
  Route::delete('delete-preset-subset-checkbox/{id}', 'Api\PresetAreaController@deletePresetSubsetCheckbox');
  Route::delete('delete-preset-subset-checkbox-option/{id}', 'Api\PresetAreaController@deletePresetSubsetCheckboxOption');
  Route::delete('delete-preset-subset-table/{id}', 'Api\PresetAreaController@deletePresetSubsetTable');
  Route::post('delete-preset-subset-table-content', 'Api\PresetAreaController@deletePresetSubsetTableContent');


  // EVALUATIONS
  Route::resource('evaluations', 'Api\SubsetEvaluationController');
  Route::post('evaluate-area/{id}', 'Api\SubsetEvaluationController@saveSubsetEvaluation');

  // EVIDENCES
  Route::resource('evidences','Api\EvidenceController');

});

Route::group(['middleware' => 'jwt.refresh'], function(){
  Route::get('auth/refresh', 'AuthController@refresh');
});
